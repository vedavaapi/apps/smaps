from vedavaapi.common.api_common import error_response


def get_vakya_ids_from_relations(sambandhas, source_vakyas=False, target_vakyas=False):
    vakya_ids = set()
    if not (source_vakyas or target_vakyas):
        return vakya_ids

    for sambandha in sambandhas:
        if source_vakyas:
            vakya_ids.add(sambandha['from'])
        if target_vakyas:
            vakya_ids.add(sambandha['to'])

    return vakya_ids


def graph(entities_colln, relations_colln, base_criteria, traversal_criteria, max_depth):
    base_criteria_for = base_criteria.get('for', 'relations')
    base_colln = {'entities': entities_colln, 'relations': relations_colln}.get(base_criteria_for, None)
    if base_colln is None:
        return error_response(message='such item type is not valid', code=403)

    base_filter = base_criteria.get('filter', None)
    if base_filter is None:
        return error_response(message='base_filter should be provided.', code=403)

    base_filter_options = base_criteria.get('options', {})

    entities = []
    relations = []
    entity_ids = set()
    relation_ids = set()

    base_entities_for_traversal = []

    if base_criteria_for == 'entities':
        base_entities_result = base_colln.find_raw(base_filter)
        base_entities = []
        for entity in base_entities_result:
            if entity['id'] not in entity_ids:
                base_entities.append(entity)
                entity_ids.add(entity['id'])
        base_entities_for_traversal = base_entities
        entities.extend(base_entities)
    else:
        base_relations = list(relations_colln.find_raw(base_filter))
        if base_filter_options.get('include_base_relations', True):
            relations.extend(base_relations)
            relation_ids.update([r['_id'] for r in base_relations])
        base_entities_filter = {
            'jsonClass': 'Vakya',
            'id': {'$in': list(get_vakya_ids_from_relations(
                base_relations, base_filter_options.get('include_source_entities', True),
                base_filter_options.get('include_target_entities', True)
            ))}
        }
        base_entities = entities_colln.find_raw(base_entities_filter)
        for e in base_entities:
            if e['id'] not in entity_ids:
                base_entities_for_traversal.append(e)
                entity_ids.add(e['id'])
        entities.extend(base_entities_for_traversal)

    if traversal_criteria is None:
        return {"entities": entities, "relations": relations}, 200

    n = 1
    while n <= max_depth:
        this_iter_entities = []
        this_iter_relations = []

        relations_filter = {
            'jsonClass': 'Sambandha',
            'from': {'$in': list(set([e['id'] for e in base_entities_for_traversal]))}
        }
        relations_filter.update(traversal_criteria)
        relations_result = relations_colln.find_raw(relations_filter)

        for relation in relations_result:
            if relation['_id'] not in relation_ids:
                this_iter_relations.append(relation)
                relation_ids.add(relation['_id'])

        entities_filter = {
            'jsonClass': 'Vakya',
            'id': {'$in': list(get_vakya_ids_from_relations(this_iter_relations, target_vakyas=True))}
        }
        entities_result = entities_colln.find_raw(entities_filter)

        for entity in entities_result:
            if entity['id'] not in entity_ids:
                this_iter_entities.append(entity)
                entity_ids.add(entity['id'])

        entities.extend(this_iter_entities)
        relations.extend(this_iter_relations)

        base_entities_for_traversal = this_iter_entities
        if not len(base_entities_for_traversal):
            break
        n = n+1

    return {'entities': entities, 'relations': relations}, 200


def pada_graph(entities_colln, relations_colln, base_sambandha_criteria, traversal_criteria, max_depth):

    entities = []
    relations = []
    entity_ids = set()
    relation_ids = set()
    pada_ids = set()

    pada_relations_map = {}

    next_iter_pada_ids = set()

    traversal_direction = traversal_criteria.get('direction', 1)
    bound_phrases = traversal_criteria.get('bound_phrase', 0)

    pada_key = ('Source_phrase', 'Target_phrase')[1-traversal_direction]
    pada_foreign_key = ('Source_phrase', 'Target_phrase')[traversal_direction]
    vakya_key = ['from', 'to'][1-traversal_direction]
    vakya_foreign_key = ['from', 'to'][traversal_direction]

    base_sambandha_filter = base_sambandha_criteria['filter']
    base_extract = base_sambandha_criteria.get('extract', 0)
    include_unbounded_base_vakyas = base_sambandha_criteria.get('include_unbounded_base_entities', 0)
    base_pada_key = ('Source_phrase', 'Target_phrase')[base_extract]
    base_vakya_key = ('from', 'to')[base_extract]

    base_sambandha_filter.update({
        "jsonClass": "Sambandha",
    })
    base_sambandha_result = list(relations_colln.find_raw(base_sambandha_filter))
    next_iter_pada_ids.update([(s[base_vakya_key] if bound_phrases else 0, s[base_pada_key]) for s in base_sambandha_result])
    pada_ids.update(next_iter_pada_ids)

    if include_unbounded_base_vakyas:
        entities_filter = {
            "jsonClass": "Vakya",
            "id": {"$in": [s[base_vakya_key] for s in base_sambandha_result]}
        }
        entities_result = list(entities_colln.find_raw(entities_filter))

        entities.extend(entities_result)
        entity_ids.update([e['id'] for e in entities_result])

    n=0

    while n < max_depth:
        this_iter__pada_ids_collection = set()
        for pada_id in next_iter_pada_ids:
            if pada_id[1] not in pada_relations_map:
                pada_relations_map[pada_id[1]] = []
            relations_filter = traversal_criteria.get("filter", {}).copy()
            relations_filter.update({
                "jsonClass": "Sambandha",
                # vakya_key: pada_id[0],
                pada_key: pada_id[1]
            })
            if bound_phrases:
                relations_filter[vakya_key] = pada_id[0]

            relations_result = list(relations_colln.find_raw(relations_filter))

            print(relations_filter, relations_result)

            this_term_vakya_ids = set()
            for relation in relations_result:
                if relation["_id"] in relation_ids:
                    continue
                relation_ids.add(relation['_id'])
                # print(relation['_id'], pada_relations_map)
                if relation['_id'] not in pada_relations_map[pada_id[1]]:
                    pada_relations_map[pada_id[1]].append(relation['_id'])
                relations.append(relation)

                pointed_vakya_id = relation[vakya_foreign_key]
                if pointed_vakya_id not in entity_ids:
                    this_term_vakya_ids.add(pointed_vakya_id)

                # if not bound_phrases:
                pointer_vakya_id = relation[vakya_key]
                if pointer_vakya_id not in entity_ids:
                    this_term_vakya_ids.add(pointer_vakya_id)

                pointed_pada_id = (relation[vakya_foreign_key] if bound_phrases else 0, relation[pada_foreign_key])

                if pointed_pada_id not in pada_ids:
                    this_iter__pada_ids_collection.add(pointed_pada_id)

            entities_filter = {
                "jsonClass": "Vakya",
                "id": {"$in": list(this_term_vakya_ids)}
            }
            entities_result = list(entities_colln.find_raw(entities_filter))

            entities.extend(entities_result)
            entity_ids.update(this_term_vakya_ids)

        next_iter_pada_ids = this_iter__pada_ids_collection
        n = n+1

    return {'entities': entities, 'relations': relations, 'phrase_relations_map': pada_relations_map}, 200



