import logging

from sanskrit_data.schema import common
from vedavaapi.common.api_common import error_response

from vedavaapi.smaps.schema import Grantha, Vakya, Sambandha


def get_sheet_values(gservices, spreadsheet_id, sheet_title):
    pargs = {
        'idType': 'title',
        'valuesFormat': 'maps'
    }
    response_table, status_code = gservices.gsheets().sheet_values_for(
        spreadsheet_id,
        sheet_title,
        pargs=pargs
    )
    return response_table, status_code


def check_if_modified(gservices, file_id, old_modified_time, if_error=True):
    try:
        file_details, status_code = gservices.gdrive().file_details(file_id)
    except Exception as e:
        logging.info('error in getting details of file with file_id {}: '.format(file_id)+str(e))
        if if_error is None:
            raise e
        # raise e
        return if_error, old_modified_time

    # print(file_details)
    modified_time_key = 'modifiedTime'
    new_modified_time = file_details[modified_time_key]

    # print(old_modified_time, new_modified_time)
    if new_modified_time != old_modified_time:  # as time value is monotonically increasing, this inequality implies, modified.
        return True, new_modified_time
    else:
        return False, new_modified_time


def import_from_gsheet(gservices, spreadsheet_id, sheet_title, my_collection, row_adapter):
    """

    :param gservices: GServices object to use to communicate with gsheets
    :param spreadsheet_id: spreadsheet_id
    :param sheet_title: title of gsheet to be imported
    :param my_collection: MyDbCollection object , as interface for collection
    :param row_adapter: a function which takes a row json_map, and returns it's specific JsonObject, and mandate of validity
    :return:
    """
    response_table, status_code = get_sheet_values(gservices, spreadsheet_id, sheet_title)
    if 'error' in response_table:
        return error_response(message='error in getting sheet details', code=404)

    missing_fields = row_adapter.missing_fields(response_table['fields'])
    if len(missing_fields):
        return error_response(message='missing fields in sheet {}: {}'.format(sheet_title, str(missing_fields)), code=422)

    if row_adapter.delete_old:
        if len(response_table['values']):
            my_collection.delete_many(row_adapter.selection)

    item_jsons = []
    for row_map in response_table['values']:
        item, validity, proceed = row_adapter.adapt(row_map)
        if (item is not None) and (validity):
            if isinstance(item, common.JsonObject):
                item_json = item.to_json_map()
            else:
                item_json = item

            item_jsons.append(item_json)

        if len(item_jsons) >= 300:
            my_collection.insert_many(item_jsons)
            item_jsons.clear()

        if not proceed:
            break

    my_collection.insert_many(item_jsons)
    item_jsons.clear()
    return {'synced': True}, 200


class GsheetRowAdapter(object):
    delete_old = False
    selection = {
        'jsonClass': 'TestObj'  # to be customised by implementation
    }

    def missing_fields(self, fields):
        return []

    def adapt(self, row):
        return None, False, False


class BasicRowAdapter(GsheetRowAdapter):
    """
    basic row adapter, can be used where each column in gsheet exactly corresponds to a key. and when no sanitization of row is required.
    """
    def __init__(self, jsonClass, selection=None, delete_old=True, essential_fields=None):
        self.jsonClass = jsonClass
        self.selection = {
            'jsonClass': self.jsonClass
        }
        if selection is not None:
            self.selection.update(selection)
        self.delete_old = delete_old
        self.essential_fields = essential_fields or []

    def missing_fields(self, fields):
        return [field for field in self.essential_fields if field not in fields]

    def adapt(self, row):
        adapted_row = row.copy()
        adapted_row['jsonClass'] = self.jsonClass
        return adapted_row, True, True


# now onwards import helpers specific to smaps

class GranthaRowAdapter(GsheetRowAdapter):
    """
    adapter for a grantha row in granthas sheet
    """

    delete_old = True
    selection = {
        'jsonClass': 'Grantha'
    }

    def __init__(self, skip_errors=True):
        super(GranthaRowAdapter, self).__init__()
        self.skip_errors = skip_errors

    def adapt(self, row):
        try:
            grantha = Grantha.from_gsheet_row(row)
        except Exception as e:
            log_string = 'row for grantha seems not valid for grantha with Book_id {}\n'.format(
                row.get('Book_id', None)) + str(e)
            logging.info(log_string)

            if self.skip_errors:
                return None, False, True
            else:
                return None, False, False
        return grantha, True, True


class VakyaRowAdapter(GsheetRowAdapter):
    delete_old = True

    def __init__(self, book_id, skip_errors=True, allow_field_defaults=True):
        super(VakyaRowAdapter, self).__init__()
        self.book_id = book_id
        self.selection = {
            'jsonClass': 'Vakya',
            'Book_id': book_id
        }
        self.skip_errors = skip_errors
        self.allow_field_defaults = allow_field_defaults

    def missing_fields(self, fields):
        essential_fields = ['Book_id', 'Vakya_id', 'Vakya']
        allowable_exceptions = ['Book_id']
        missing_fields = [field for field in essential_fields if field not in fields]
        if self.allow_field_defaults:
            missing_fields = [field for field in missing_fields if field not in allowable_exceptions]
        return missing_fields

    def adapt(self, row):
        try:
            vakya = Vakya.from_gsheet_row(
                row,
                default_book_id=(self.book_id if self.allow_field_defaults else None)
            )
        except Exception as e:
            log_string = 'row for vakya seems not valid for vakya with Vakya_id:{}, Book_id:{}; reason:'.format(
                row.get('Vakya_id', None), row.get('Book_id', self.book_id)) + str(e)
            logging.info(log_string)

            if self.skip_errors:
                return None, False, True
            else:
                return None, False, False

        return self._adapt_for_vis(vakya.to_json_map()), True, True

    @staticmethod
    def _adapt_for_vis(vakya):
        """
        it will adapt for vis network conventions. not part of interface. just helper method in this class.
        :param vakya:
        :return:
        """
        vakya = vakya.copy()  # to avoid side affects

        vakya['title'] = vakya['Vakya']
        vakya['label'] = '{id_part}: {title_part}'.format(id_part=vakya['Vakya_id'], title_part=vakya['Vakya'][:5])

        return vakya


class SambandhaRowAdapter(GsheetRowAdapter):
    delete_old = True

    def __init__(self, source_book_id, skip_errors=True, allow_field_defaults=True):
        super(SambandhaRowAdapter, self).__init__()
        self.source_book_id = source_book_id
        self.selection = {
            'jsonClass': 'Sambandha',
            'Source_book_id': source_book_id
        }
        self.skip_errors = skip_errors
        self.allow_field_defaults = allow_field_defaults

    def missing_fields(self, fields):
        essential_fields = ['Source_book_id', 'Source_vakya_id', 'Source_phrase', 'Target_book_id', 'Target_vakya_id', 'Target_phrase']
        allowable_exceptions = ['Source_book_id', 'Target_book_id']
        missing_fields = [field for field in essential_fields if field not in fields]
        if self.allow_field_defaults:
            missing_fields = [field for field in missing_fields if field not in allowable_exceptions]
        return missing_fields

    def adapt(self, row):
        try:
            sambandha = Sambandha.from_gsheet_row(
                row,
                default_book_id=(self.source_book_id if self.allow_field_defaults else None)
            )
        except Exception as e:
            log_string = 'row for sambandha seems not valid for sambandha with Source_vakya_id:{}, Source_book_id:{}, Target_vakya_id:{}, Target_book_id:{}; reason:'.format(row.get('Source_vakya_id', 'undef'), row.get('Source_book_id', self.source_book_id), row.get('Target_vakya_id', 'undef'), row.get('Target_book_id', self.source_book_id)) + str(e)
            logging.info(log_string)

            if self.skip_errors:
                return None, False, True
            else:
                return None, False, False
        return self._adapt_for_vis(sambandha.to_json_map()), True, True

    @staticmethod
    def _adapt_for_vis(sambandha):
        #  see Vakya_row_adapter documentation
        sambandha = sambandha.copy()

        sambandha['label'] = '{sp} → {tp}'.format(sp=sambandha['Source_phrase'], tp=sambandha['Target_phrase'])

        sambandha_tags = [key for key in sambandha.keys() if key.endswith('_tag')]
        sambandha['title'] = '<br>'.join([t.replace('_tag', '')+':'+sambandha[t] for t in sambandha_tags])

        return sambandha



