import json

import flask_restplus
# from flask_restplus import Namespace

from vedavaapi.common.api_common import get_current_org

from .. import myservice, helper
from . import api



@api.route('/entities')
class Entities(flask_restplus.Resource):
    post_parser = api.parser()
    post_parser.add_argument('filter', location='form', required=True)

    @api.expect(post_parser, validate=True)
    def post(self):
        args = self.post_parser.parse_args()
        entities = myservice().entities_colln(get_current_org()).find(
            json.loads(args.get('filter'))
        )
        return list(entities), 200


@api.route('/relations')
class Relations(flask_restplus.Resource):
    post_parser = api.parser()
    post_parser.add_argument('filter', location='form', required=True)

    @api.expect(post_parser, validate=True)
    def post(self):
        args = self.post_parser.parse_args()
        relations = myservice().relations_colln(get_current_org()).find(
            json.loads(args.get('filter'))
        )
        return list(relations), 200


@api.route('/graph')
class Graph(flask_restplus.Resource):
    post_parser = api.parser()
    post_parser.add_argument('base_criteria', location='form', type=str, required=True)
    post_parser.add_argument('traversal_criteria', location='form', default=None)
    post_parser.add_argument('max_depth', location='form', type=int, default=1)

    @api.expect(post_parser, validate=True)
    def post(self):
        args = self.post_parser.parse_args()
        graph, status_code = helper.graph(
            myservice().entities_colln(get_current_org()),
            myservice().relations_colln(get_current_org()),
            base_criteria=json.loads(args.get('base_criteria')),
            traversal_criteria=json.loads(args.get('traversal_criteria')) if args.get('traversal_criteria') else None,
            max_depth=args.get('max_depth')
        )
        return graph, status_code

@api.route('/term_graph')
class TermGraph(flask_restplus.Resource):
    post_parser = api.parser()
    post_parser.add_argument('base_sambandha_criteria', location='form', type=str, required=True)
    post_parser.add_argument('traversal_criteria', location='form', required=True)
    post_parser.add_argument('max_depth', location='form', type=int, default=1)

    @api.expect(post_parser, validate=True)
    def post(self):
        args = self.post_parser.parse_args()
        graph, status_code = helper.pada_graph(
            myservice().entities_colln(get_current_org()),
            myservice().relations_colln(get_current_org()),
            base_sambandha_criteria=json.loads(args.get('base_sambandha_criteria')),
            traversal_criteria=json.loads(args.get('traversal_criteria')),
            max_depth=args.get('max_depth')
        )
        return graph, status_code

