from .. import VedavaapiSmaps


def myservice():
    return VedavaapiSmaps.instance


from .v1 import api_blueprint_v1

blueprints_path_map = {
    api_blueprint_v1: '/v1'
}
