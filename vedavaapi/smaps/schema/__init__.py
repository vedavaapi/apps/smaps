import re
import sys

from sanskrit_data.schema import common


def is_filler_string(string):
    if re.match(r'^[\s\-]*?$', string):
        return True
    return False


def is_undefined_or_filler(row, field):
    if field not in row:
        return True
    if is_filler_string(row[field]):
        return True
    return False


class Grantha(common.JsonObject):
    schema = common.recursively_merge_json_schemas(
        common.JsonObject.schema,
        ({
            "type": "object",
            "description": "models a Grantha for smaps, contains it's minimal metadata now.",
            "properties": {
                common.TYPE_FIELD: {
                    "enum": ["Grantha"]
                },
                "vakyas_sheet_fields": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "vakyas_sheet_field_descriptions": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "sambandhas_sheet_fields": {
                    "type": "string",
                    "items": {
                        "type": "string"
                    }
                },
                "sambandhas_sheet_field_descriptions": {
                    "type": "string",
                    "items": {
                        "type": "string"
                    }
                },
                "modified_time": {
                    # will be used to check wether to sync or not.
                    "type": "string",  # TODO should be changed to int and unix timestamp
                },
                "Spreadsheet_id": {
                    "type": "string",
                    "description": "google sheets spreadsheet_id corresponding to this grantha"
                },
                "Book_id": {
                    "type": "string",
                    "description": "our id of this grantha. it will be used in referances like Book_id, etc. constant "
                                   "wether synced from gsheets or local XL sheets, or from where ever "
                },
                "Book_title": {
                    "type": "string"
                },
                "Status": {
                    "type": "string",
                    "description": "status of shaastra."
                }
            },

            "required": ["Spreadsheet_id", "Book_id", "Book_title", "Status"]
        })
    )

    @classmethod
    def from_gsheet_row(cls, row, include_fillers=False, extra_details=None):
        grantha = cls()
        essential_fields = ["Spreadsheet_id", "Book_id", "Book_title", "Status"]
        missing_fields = [field for field in essential_fields if is_undefined_or_filler(row, field)]

        if len(missing_fields):
            raise KeyError("missed essential keys : {}".format(str(missing_fields)))

        for key, value in row.items():
            if re.match(r'^[\s\-]*?$', key):
                continue
            if not is_filler_string(value) or include_fillers:
                setattr(grantha, key, value)

        if extra_details is None:
            extra_details = {}

        for key, value in extra_details.items():
            setattr(grantha, key, value)  # like 'modified_time' computed from other sources

        grantha.validate()
        return grantha


def effective_vakya_id(book_id, vakya_id):
    """
    convention to create effective id of a vakya from it's components is abstracted in this.
    presently both components are conc'd with ':' in between.
    :param book_id:
    :param vakya_id:
    :return:
    """
    return ':'.join((book_id, vakya_id))


class Vakya(common.JsonObject):
    schema = common.recursively_merge_json_schemas(common.JsonObject.schema, ({
        "type": "object",
        "description": "a vakya",
        "properties": {
            common.TYPE_FIELD: {
                "enum": ["Vakya"],
            },
            "Book_id": {
                "type": "string",
            },
            "Vakya_id": {
                "type": "string"
            },
            "Vakya": {
                "type": "string"
            }
        },
        "required": ["Book_id", "Vakya_id", "Vakya"]
    }))

    # noinspection PyUnresolvedReferences
    @classmethod
    def from_gsheet_row(cls, row, include_fillers=False, default_book_id=None):
        vakya = cls()
        essential_fields = ["Book_id", "Vakya_id", "Vakya"]

        if default_book_id is not None:
            if is_undefined_or_filler(row, 'Book_id'):
                row['Book_id'] = default_book_id

        missing_fields = [field for field in essential_fields if is_undefined_or_filler(row, field)]
        if len(missing_fields):
            raise KeyError("missed essential keys : {}".format(str(missing_fields)))

        for key, value in row.items():
            if re.match(r'^[\s\-]*?$', key):
                continue
            if not is_filler_string(value) or include_fillers:
                setattr(vakya, key, value)

        vakya.validate()
        setattr(vakya, 'id', effective_vakya_id(vakya.Book_id, vakya.Vakya_id))
        return vakya


class Sambandha(common.JsonObject):
    schema = common.recursively_merge_json_schemas(common.JsonObject.schema, ({
        "type": "object",
        "description": "a sambandha",
        "properties": {
            common.TYPE_FIELD: {
                "enum": ["Sambandha"],
            },
            "Source_book_id": {
                "type": "string",
            },
            "Source_vakya_id": {
                "type": "string"
            },
            "Vakya": {
                "type": "string"
            },
            "Source_phrase": {
                "type": "string"
            },
            "Target_book_id": {
                "type": "string"
            },
            "Target_vakya_id": {
                "type": "string"
            },
            "Target_phrase": {
                "type": "string"
            }
        },
        "required": ["Source_book_id", "Source_vakya_id", "Source_phrase", "Target_book_id", "Target_vakya_id",
                     "Target_phrase"]
    }))

    @classmethod
    def from_gsheet_row(cls, row, include_fillers=False, default_book_id=None):
        sambandha = cls()
        essential_fields = ["Source_book_id", "Source_vakya_id", "Source_phrase", "Target_book_id", "Target_vakya_id", "Target_phrase"]

        if default_book_id is not None:
            if is_undefined_or_filler(row, 'Source_book_id'):
                row['Source_book_id'] = default_book_id
            if is_undefined_or_filler(row, 'Target_book_id'):
                row['Target_book_id'] = default_book_id

        missing_fields = [field for field in essential_fields if is_undefined_or_filler(row, field)]
        if len(missing_fields):
            raise KeyError("missed essential keys : {}".format(str(missing_fields)))

        for key, value in row.items():
            if re.match(r'^[\s\-]*?$', key):
                continue
            if not is_filler_string(value) or include_fillers:
                setattr(sambandha, key, value)

        sambandha.validate()
        setattr(sambandha, 'from', effective_vakya_id(sambandha.Source_book_id, sambandha.Source_vakya_id))
        setattr(sambandha, 'to',  effective_vakya_id(sambandha.Target_book_id, sambandha.Target_vakya_id))
        return sambandha


common.update_json_class_index(sys.modules[__name__])
