var Helper = /** @class */ (function () {
    function Helper() {
    }
    Helper.getQueryParameters = function () {
        return document.location.search.replace(/(^\?)/, '').split("&").map(function (n) { return n = n.split("="), this[n[0]] = n[1], this; }.bind({}))[0];
    };
    Helper.getSelectedTextRectangle = function () {
        var selection = window.getSelection();
        var range = selection.getRangeAt(0);
        var rect = range.getBoundingClientRect();
        return rect;
    };
    Helper.excludeFields = function (key, value) {
        var nolist = new Set(['from', 'to', 'id', 'arrows', 'font',
            'Target_phrase', 'Source_phrase', '_id', 'Vakya', 'jsonClass', 'group',
            'Target_vakya_id', 'Source_vakya_id', 'Source_book_id', 'Target_book_id', 'Book_id', 'Vakya_id', 'shape', 'color']);
        if (nolist.has(key)) {
            return undefined;
        }
        return value;
    };
    return Helper;
}());
