// Extended Vis.js Network Class
var EVisNetwork = /** @class */ (function () {
    function EVisNetwork(network, graph, networkId) {
        this.network = network;
        this.nodes = graph.nodes;
        this.edges = graph.edges;
        this.networkId = networkId;
    }
    EVisNetwork.prototype.matchNodeByTitle = function (regexp) {
        return this.nodes.get({
            filter: function (item) {
                return item.title.match(regexp);
            }
        });
    };
    EVisNetwork.prototype.searchNodeByTitle = function (key) {
        return this.matchNodeByTitle(new RegExp(".*" + key + ".*"))[0];
    };
    EVisNetwork.prototype.zoomToNode = function (nodeID, scaleArg) {
        this.network.moveTo({
            position: this.network.getPositions([nodeID,])[nodeID],
            scale: scaleArg,
            offset: { x: 0, y: 0 },
            animation: {
                duration : 250,
                easingFunction: "easeInOutQuad"
            }
        });
    };
    EVisNetwork.prototype.changeNodeColor = function (nodeID, prefColor = false) {
        var color = {
            border:  prefColor ? prefColor : '#2b7ce9',
            background: prefColor ? prefColor :  '#d2e5ff',
            highlight: {
              border: prefColor ? prefColor :  '#2b7ce9',
              background: prefColor ? prefColor :  '#d2e5ff'
            },
            hover: {
              border: prefColor ? prefColor :  '#2b7ce9',
              background: prefColor ? prefColor :  '#d2e5ff'
            }
        }
        this.nodes.update({ id: nodeID, color: color });
    };
    EVisNetwork.prototype.changeNodeShape = function (nodeID, shape) {
        this.nodes.update({ id: nodeID, shape: shape });
    };
    EVisNetwork.prototype.changeNodeFont = function (nodeID, fontSize = false, fontFace = false, fontColor = false) {
        var font = {
            color: fontColor ? fontColor : '#343434',
            size: fontSize ? fontSize : 14,
            face: fontFace ? fontFace : 'arial',
            background: undefined,
            strokeWidth: 0,
            strokeColor: '#ffffff',
            align: 'center',
            vadjust: 0,
            multi: false
        }
        this.nodes.update({ id: nodeID, font: font });
    };

    EVisNetwork.prototype.unHighlightNodes = function () {
        var oldNodes = this.nodes.get({
            filter: function (item) {
                return item.shape === 'box';
            }
        });
        oldNodes.forEach(oldNode => {
            var oldNodeID = oldNode.id;
            this.changeNodeShape(oldNodeID, this.network.isCluster(oldNodeID) ? "dot" : "ellipse");
            this.changeNodeFont(oldNodeID);
            this.changeNodeColor(oldNodeID);
        })
    }

    EVisNetwork.prototype.highlightNode = function (nodeID) {
        this.unHighlightNodes();
        // this.backupNodeProperties(nodeID, ["shape", "color", "font"]);
        this.changeNodeShape(nodeID, "box");
        this.changeNodeFont(nodeID, 14, "arial", "#fff");
        // this.changeNodeColor(nodeID, "#800000");
        //this.network.fit();
    };

    EVisNetwork.prototype.backupNodeProperties = function(nodeId, propertyNames) {
        let modNode = this.nodes.get(nodeId);
        if(!(modNode.hasOwnProperty('propBackup'))) {
            modNode['propBackup'] = {};
        }
        propertyNames.forEach((prop, i) => {
            if(modNode.hasOwnProperty(prop)) {
                modNode.propBackup[prop] = modNode[prop];
            }
        });
        return modNode;
    };

    EVisNetwork.prototype.restoreProps = function(nodeId, propertyNames) {
        let modNode = this.nodes.get(nodeId);
        if (!(modNode.hasOwnProperty('propBackup'))) {
            modNode['propBackup'] = {};
        }
        propertyNames.forEach((prop, i) => {
            if(modName.propBackup.hasOwnProperty(prop)) {
                modNode[prop] = modNode.propBackup[prop];
            }
        });
        return modNode;
    };

    EVisNetwork.prototype.clusterByColor = function() {
        /*let colorsSet = new Set();
        for (let nodeId of Object.keys(this.network.body.nodes)) {
            if(nodeId.startsWith('edgeId:')) {
                continue
            }
            node = this.network.body.nodes[nodeId];
            let backgroundColor = node.options.color.background;
            if(backgroundColor) {
                colorsSet.add(backgroundColor);
            }
        }
        let colors = [];
        for(let color of colorsSet) {
            colors.push(color);
        }
        console.log({colors, colorsSet});*/
        let groups = ["ndefault", "disabled", "base"];
        var clusterOptionsByData;
        for (var i = 0; i < groups.length; i++) {
            var group = groups[i];
            console.log({color: group});
            clusterOptionsByData = {
                joinCondition: function (childOptions) {
                    // console.log({childOptions});
                    /*if(childOptions.shape == 'database') {
                        return false;
                    }*/
                    return childOptions.group == group; // the color is fully defined in the node.
                },
                processProperties: function (clusterOptions, childNodes, childEdges) {
                    var totalMass = 0;
                    for (var i = 0; i < childNodes.length; i++) {
                        totalMass += childNodes[i].mass;
                    }
                    clusterOptions.mass = 5;
                    return clusterOptions;
                },
                clusterNodeProperties: { id: 'cluster:' + group, borderWidth: 3, shape: 'dot', label: 'cluster:' + i, group: group }
            };
            this.network.cluster(clusterOptionsByData);
            // break;
        }
    };

    EVisNetwork.prototype.clusterByHubSize = function clusterByHubsize() {
        let n = Math.floor(this.nodes.length / 10) + 1;
        var clusterOptionsByData = {
            processProperties: function (clusterOptions, childNodes) {
                clusterOptions.label = "[" + childNodes.length + "]";
                return clusterOptions;
            },
            clusterNodeProperties: { borderWidth: 3, shape: 'box', group: "cluster" }
        };
        //for(let i=0; i<n; i++) {
            this.network.clusterByHubsize(
              n,
              clusterOptionsByData
            );
        //}
    }

    // cluster of clusters
    EVisNetwork.prototype.clusterByCustom2 = function (p_clusterSize) {

        let clusterSize = p_clusterSize || 30;
        let n = this.nodes.length; // includes all the nodes even within clusters
        let level = 2;
        let clusterIds = [];
        for (let nodeId of Object.keys(this.network.body.nodes)) {
            if(this.network.isCluster(nodeId)) {
                clusterIds.push(nodeId);
            }
        }

        const nc = Math.floor(clusterIds.length / clusterSize) + 1;
        let lastId = 0;
        for (var i=0; i<nc; i++) {
            let noOfItems = 0;
            clusterOptionsByData = {
                joinCondition: function (childOptions) {
                    if(!clusterIds.includes(childOptions.id)) {
                        return false;
                    }
                    noOfItems = noOfItems + 1
                    console.log("cluster this", childOptions.id);

                    if(noOfItems == clusterSize) {
                        lastId++;
                        level++;
                        noOfItems = noOfItems % clusterSize;
                    }
                    return true;
                },
                processProperties: function (clusterOptions, childNodes, childEdges) {
                    clusterOptions.mass = 10;
                    return clusterOptions;
                },
                clusterNodeProperties: { id: `cluster_${level}:${i}`, borderWidth: 3, shape: 'star', label: `cluster_${level}`, group: `cluster_${level}`, font: {size: '24'}, size: 25, color: '#ff9933' }
                };
            this.network.cluster(clusterOptionsByData);
        }
    }

    EVisNetwork.prototype.clusterByCustom = function (p_clusterSize) {
        function normalizedId(id) {
            let idParts = id.split('.');
            let normalizedIdParts = idParts.map((ip) => ip.padStart(4, '0'));
            return normalizedIdParts.join('.');
        }

        let clusterSize = p_clusterSize || 30;
        let n = this.nodes.length; // includes all the nodes even within clusters
        let toClusterIds = [];
        let nodesInCluster = [];

        for (let nodeId of Object.keys(this.network.body.nodes)) {
            if(this.network.isCluster(nodeId)) {
                nodesInCluster.push(...this.network.getNodesInCluster(nodeId));
            }
            else {
                toClusterIds.push(nodeId);
            }
        }

        const nc = Math.floor(toClusterIds.length / clusterSize) + 1;
        console.log("NC=",nc);
        let lastId = 0;
        for (var i=0; i<nc; i++) {
            let noOfItems = 0;
            clusterOptionsByData = {
                joinCondition: function (childOptions) {
                    if(!toClusterIds.includes(childOptions.id) || nodesInCluster.includes(childOptions.id)) {
                        return false;
                    }
                    if(!childOptions.hasOwnProperty('Vakya_id')) {
                        return false;
                    }
                    let normalizedVakyaId = normalizedId(childOptions['Vakya_id']);
                    if(normalizedVakyaId < lastId) {
                        return false;
                    }
                    noOfItems = noOfItems + 1;
                    if(noOfItems == clusterSize) {
                        lastId = normalizedVakyaId;
                    }
                    return noOfItems <= clusterSize; // the color is fully defined in the node.
                },
                processProperties: function (clusterOptions, childNodes, childEdges) {
                    var totalMass = 0;
                    for (var i = 0; i < childNodes.length; i++) {
                        totalMass += childNodes[i].mass;
                    }
                    clusterOptions.mass = 10;
                    return clusterOptions;
                },
                clusterNodeProperties: { id: `cluster_2_${lastId}:${i}`, borderWidth: 3, shape: 'star', label: `cluster_2_${lastId}`, group: `cluster_2`, font: {size: '24'}, size: 25, color: '#ff9933' }
                };
            this.network.cluster(clusterOptionsByData);
        }

//         this.network.redraw();

        // cluster the remaining nodes into one box
        toClusterIds = [];
        nodesInCluster = [];

        for (let nodeId of Object.keys(this.network.body.nodes)) {
            if(this.network.isCluster(nodeId)) {
                nodesInCluster.push(...this.network.getNodesInCluster(nodeId));
            }
            else {
                toClusterIds.push(nodeId);
            }
        }
//         console.log("cluster remaining: ", toClusterIds.length);
        let noOfItems = 0;
        clusterOptionsByData = {
            joinCondition: function (childOptions) {
                if(!toClusterIds.includes(childOptions.id) ||nodesInCluster.includes(childOptions.id)) {
                    return false;
                }
                noOfItems = noOfItems + 1;
                return true;
            },
            processProperties: function (clusterOptions, childNodes, childEdges) {
                var totalMass = 0;
                for (var i = 0; i < childNodes.length; i++) {
                    totalMass += childNodes[i].mass;
                }
                clusterOptions.mass = 10;
                return clusterOptions;
            },
            clusterNodeProperties: { id: `cluster_3`, borderWidth: 3, shape: 'star', label: `cluster_3`, group: `cluster_3`, font: {size: '24'}, size: 25, color: '#90EE90' }
            };
        this.network.cluster(clusterOptionsByData);
    }

    EVisNetwork.prototype.clusterBySequences = function (p_clusterSize) {
        function normalizedId(id) {
            let idParts = id.split('.');
            let normalizedIdParts = idParts.map((ip) => ip.padStart(4, '0'));
            return normalizedIdParts.join('.');
        }
        let clusterSize = p_clusterSize || 30;
        console.log("clusterSize = " + clusterSize);
        let n = Math.floor(this.nodes.length / clusterSize) + 1;
        let lastId = '0';
        for (var i = 0; i < n; i++) {
            let noOfItems = 0;
            clusterOptionsByData = {
                joinCondition: function (childOptions) {
                    // console.log({ childOptions });
                    if(!childOptions.hasOwnProperty('Vakya_id')) {
                        return false;
                    }
                    let normalizedVakyaId = normalizedId(childOptions['Vakya_id']);
                    if(normalizedVakyaId < lastId) {
                        return false;
                    }
                    noOfItems = noOfItems + 1;
                    if(noOfItems == clusterSize) {
                        lastId = normalizedVakyaId;
                    }
                    return noOfItems <= clusterSize; // the color is fully defined in the node.
                },
                processProperties: function (clusterOptions, childNodes, childEdges) {
                    var totalMass = 0;
                    for (var i = 0; i < childNodes.length; i++) {
                        totalMass += childNodes[i].mass;
                    }
                    clusterOptions.mass = 5;
                    return clusterOptions;
                },
                clusterNodeProperties: { id: 'cluster_1:' + i, borderWidth: 3, shape: 'dot', label: `${lastId.replace(/^0+/g, '').replace(/\.0+/g, '.') || '0'}`, group: 'cluster_1', font: {size: '24'}, size: 25, color: '#ff9933' }
            };
            this.network.cluster(clusterOptionsByData);
            // break;
        }
    };

    EVisNetwork.prototype.clusterByNone = function() {
        for (let nodeId of Object.keys(this.network.body.nodes)) {
            if (nodeId.startsWith('edgeId:')) {
                continue
            }
            if(this.network.isCluster(nodeId)) {
                try {
                    this.network.openCluster(nodeId);
                }
                catch {}
            }
        }
    };

    EVisNetwork.prototype.clusterByOutLiers = function clusterOutliers() {
        let clusterOptionsByData = {
            clusterNodeProperties: { id: 'cluster:' + String(Math.random()), borderWidth: 3, shape: 'dot', label: 'cluster', group: 'cluster', font: { size: '24' }, size: 25 }
        };
        this.network.clusterOutliers(clusterOptionsByData);
    }

    return EVisNetwork;
}());
