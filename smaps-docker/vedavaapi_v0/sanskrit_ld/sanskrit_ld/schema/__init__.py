import json
import logging
import sys
from copy import deepcopy

import jsonpickle
import jsonschema
from jsonschema import SchemaError
from jsonschema.exceptions import best_match, ValidationError

CLASS_FIELD = 'jsonClass'
JSONPICKLE_TYPE_FIELD = "py/object"

json_class_registry = {}


class DotDict(dict):

    def __getattr__(self, item):
        return self.get(item)


def updata_json_class_registry(module_in):
    import inspect
    for name, obj in inspect.getmembers(module_in):
        if inspect.isclass(obj):
            json_class_registry[name] = obj


def check_class(obj, allowed_classes):
    results = [isinstance(obj, cls) for cls in allowed_classes]
    return True in results


def check_list_item_classes(items, allowed_classes):
    check_class_results = [check_class(item, allowed_classes=allowed_classes) for item in items]
    return not (False in check_class_results)


def recursively_merge_json_schemas(a, b, json_path=""):
    assert a.__class__ == b.__class__, str(a.__class__) + " vs " + str(b.__class__)

    if isinstance(b, dict) and isinstance(a, dict):
        a_and_b = set(a.keys()) & set(b.keys())
        every_key = set(a.keys()) | set(b.keys())
        merged_dict = {}
        for k in every_key:
            if k in a_and_b:
                merged_dict[k] = recursively_merge_json_schemas(a[k], b[k], json_path=json_path + "/" + k)
            else:
                merged_dict[k] = deepcopy(a[k] if k in a else b[k])
        return merged_dict
    elif isinstance(b, list) \
            and isinstance(a, list) \
            and not (json_path.endswith(CLASS_FIELD + "/enum") or json_path.endswith('type' + "/enum")):
        # TODO: What if we have a list of dicts?
        items_are_dicts = (len(a) and isinstance(a[0], dict)) or (len(b) and isinstance(b[0], dict))
        return list(set(a + b)) if not items_are_dicts else b
    else:
        return deepcopy(b)


def merge_contexts(*contexts):
    merged_context = {}
    for context in contexts:
        merged_context.update(context)
    return merged_context


# now on base classes for sanskrit_ld

class JsonObject(object):
    json_class = 'JsonObject'

    context = {
        "_id": "@id",
        "type": "@type",
        "canonical": {
            "@id": "oa:canonical"
        },
        "via": {
            "@id": "oa:via"
        },
    }

    schema = {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["JsonObject"]
            },
            "type": {
                "enum": ["owl:Thing"]
            },
            "_id": {
                "type": "string"
            },
            "canonical": {
                "type": "string"
            },
            "via": {
                "type": "string"
            },
            "jsonClassLabel": {
                "type": "string"
            }
        },
        "dependencies": {
            "canonical": ["via"],
            "via": ["canonical"]
        },
        "required": ["type"]
    }

    def __init__(self):
        self.set_class_field()
        self.set_type_field()

    def set_class_field(self):
        setattr(self, CLASS_FIELD, self.__class__.json_class)

    def set_type_field(self):
        setattr(self, 'type', self.__class__.schema['properties']['type']['enum'][0])

    def set_details(self, *args, **kwargs):
        pass

    @classmethod
    def from_details(cls, *args, **kwargs):
        obj = cls()
        obj.set_details(*args, **kwargs)
        return obj

    @classmethod
    def get_jsonpickle_typeid(cls):
        return cls.__module__ + "." + cls.__name__

    def set_class_field_recursively(self):
        self.set_class_field()
        for key, value in iter(self.__dict__.items()):
            if isinstance(value, JsonObject):
                value.set_class_field_recursively()
            elif isinstance(value, list):
                for item in value:
                    if isinstance(item, JsonObject):
                        item.set_class_field_recursively()

    def set_type_field_recursively(self):
        self.set_type_field()
        for key, value in iter(self.__dict__.items()):
            if isinstance(value, JsonObject):
                value.set_type_field_recursively()
            elif isinstance(value, list):
                for item in value:
                    if isinstance(item, JsonObject):
                        item.set_type_field_recursively()

    def set_jsonpickle_type_recursively(self):
        self.set_class_field()
        for key, value in iter(self.__dict__.items()):
            if isinstance(value, JsonObject):
                value.set_jsonpickle_type_recursively()
            elif isinstance(value, list):
                for item in value:
                    if isinstance(item, JsonObject):
                        item.set_jsonpickle_type_recursively()

    @classmethod
    def make_from_dict(cls, input_dict):
        """

        :type input_dict: dict
        """
        if input_dict is None:
            return None
        assert CLASS_FIELD in input_dict, "no type field: " + str(input_dict)
        dict_without_id = deepcopy(input_dict)  # type: dict
        _id = dict_without_id.pop('_id', None)

        def recursively_set_jsonpickle_type(obj):
            if isinstance(obj, dict):
                wire_type = obj.pop(CLASS_FIELD, None)
                if wire_type:
                    obj[JSONPICKLE_TYPE_FIELD] = '.'.join((json_class_registry[wire_type].__module__, wire_type))
                else:
                    obj[JSONPICKLE_TYPE_FIELD] = "object"

                for k, v in obj.items():
                    recursively_set_jsonpickle_type(v)
            elif isinstance(obj, list):
                for item in obj:
                    recursively_set_jsonpickle_type(item)

        recursively_set_jsonpickle_type(dict_without_id)

        new_obj = jsonpickle.decode(json.dumps(dict_without_id))  # type: JsonObject
        if _id is not None:
            setattr(new_obj, '_id', _id)
        new_obj.set_class_field_recursively()
        new_obj.set_type_field_recursively()
        return new_obj

    @classmethod
    def make_from_dict_list(cls, input_dict_list):
        """

        :type input_dict_list: list
        """
        assert isinstance(input_dict_list, list)
        return [cls.make_from_dict(input_dict=input_dict) for input_dict in input_dict_list]

    @classmethod
    def make_from_pickle(cls, pickle):
        input_str = pickle
        if not isinstance(pickle, str):
            input_str = str(pickle)
        if input_str.strip().startswith('['):
            return cls.make_from_dict_list(jsonpickle.decode(pickle))
        else:
            return cls.make_from_dict(jsonpickle.decode(pickle))

    def set_from_dict(self, input_dict):
        """

        :type input_dict: dict
        """
        if input_dict is None:
            return
        for k, v in input_dict.items():
            if v is None:
                continue
            if isinstance(v, dict):
                setattr(self, k, JsonObject.make_from_dict(v))
            elif isinstance(v, list):
                setattr(self, k, [JsonObject.make_from_dict(item) if isinstance(item, dict) else item for item in v])
            else:
                setattr(self, k, v)

    def to_json_map(self):
        self.set_class_field_recursively()
        json_map = {}

        for k, v in self.__dict__.items():
            if isinstance(v, JsonObject):
                json_map[k] = v.to_json_map()
            elif isinstance(v, list):
                json_map[k] = [item.to_json_map() if isinstance(item, JsonObject) else item for item in v]
            else:
                json_map[k] = v
        return json_map

    def equals_ignore_id(self, other):
        """

        :type other: JsonObject
        """
        dict1 = self.to_json_map()  # type:dict
        dict1.pop('_id', None)
        dict2 = other.to_json_map()  # type: dict
        dict2.pop('_id', None)
        return dict1 == dict2

    def validate_property_types(self):
        """
        this should be called after first level structural schema validation.
        TODO can optimize after
        :return:
        """
        for k, v in self.__dict__.items():
            if k not in self.schema['properties']:
                continue
            property_schema = self.schema['properties'][k]
            matched_schema = None

            is_json_object = isinstance(v, JsonObject)
            is_list = isinstance(v, list)

            if not (is_json_object or is_list):
                continue

            if 'type' in property_schema:
                matched_schema = property_schema
            elif 'anyOf' in property_schema or 'oneOf' in property_schema:
                opt_schemas = property_schema.get('anyOf', property_schema.get('oneOf'))
                # noinspection PyUnresolvedReferences
                opt_schemas_short_listed = [
                    o for o in opt_schemas
                    if o.get('type') == ('object' if is_json_object else 'array')
                ]

                if len(opt_schemas_short_listed) == 1:
                    matched_schema = opt_schemas_short_listed[0]
                    logging.debug('only one shortlisted')
                else:
                    for opt_schema in opt_schemas_short_listed:
                        try:
                            if is_json_object:
                                jsonschema.validate(v.to_json_map(), opt_schema)
                                matched_schema = opt_schema
                                break
                            elif is_list:
                                # noinspection PyTypeChecker
                                jsonschema.validate(v[0].to_json_map(), opt_schema['items'])
                                matched_schema = opt_schema
                                break

                        except (ValidationError, TypeError) as e:
                            logging.error(e)
                            pass

            if matched_schema is None:
                continue  # TODO should handle it other way

            if is_json_object:
                allowed_class_names = matched_schema.get('allowedClasses', ['JsonObject'])
                allowed_classes = [
                    json_class_registry.get(clsname) for clsname in allowed_class_names
                    if clsname in json_class_registry
                ]
                if not check_class(v, allowed_classes):
                    raise TypeError('invalid object type for property {}'.format(k))
            elif is_list:
                item_schema = matched_schema.get('items')
                item_type = item_schema.get('type', 'string')
                if item_type is 'object':
                    allowed_class_names = item_schema.get('allowedClasses', ['JsonObject'])
                    allowed_classes = [
                        json_class_registry.get(clsname) for clsname in allowed_class_names
                        if clsname in json_class_registry
                    ]
                    if not check_list_item_classes(v, allowed_classes):
                        raise TypeError('invalid object type for an item in list {}'.format(k))

    def validate_schema(self):
        json_map = self.to_json_map()
        json_map.pop('_id', None)

        try:
            jsonschema.validate(json_map, self.schema)
            self.validate_property_types()
            for k, v in self.__dict__.items():
                if isinstance(v, JsonObject):
                    v.validate_schema()
                elif isinstance(v, list):
                    for item in v:
                        if isinstance(item, JsonObject):
                            item.validate_schema()
                else:
                    pass
        except SchemaError as e:
            logging.error("Exception message: " + e.message)
            logging.error("Schema is: " + jsonpickle.dumps(self.schema))
            logging.error("Context is: " + str(e.context))
            logging.error("Best match is: " + str(best_match(errors=[e])))
            raise e
        except ValidationError as e:
            logging.error("Exception message: " + e.message)
            logging.error("self is: " + str(self))
            logging.error("Schema is: " + jsonpickle.dumps(self.schema))
            logging.error("Context is: " + str(e.context))
            logging.error("Best match is: " + str(best_match(errors=[e])))
            logging.error("json_map is: " + jsonpickle.dumps(json_map))
            raise e

    def validate(self):
        self.validate_schema()


class WrapperObject(JsonObject):
    """
    owl:Thing
    """
    json_class = 'WrapperObject'

    schema = recursively_merge_json_schemas(JsonObject.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["WrapperObject"]
            },
            "type": {
                "enum": ["owl:Thing"]
            }
        }
    })

    def set_details(self, **kwargs):
        self.set_from_dict(kwargs)


updata_json_class_registry(sys.modules[__name__])


from . import base, users, books
from .base import annotations, agents, selectors
