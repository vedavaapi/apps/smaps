import sys

from .. import CLASS_FIELD, updata_json_class_registry
from .. import recursively_merge_json_schemas, merge_contexts
from ..base import Resource, SpecificResource


class BookPortion(Resource):

    """
    dcterms:BibliographicResource
    """
    json_class = 'BookPortion'

    context = merge_contexts(Resource.context, {
        "bibClass": {
            "@id": "sld:bibliographicClass",
            "@type": "sld:BibliographicClass"
        },
        "bibClassLabel": {
            "@id": "sld:bibliographicClassLabel"
        }
    })

    schema = recursively_merge_json_schemas(Resource.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["BookPortion"]
            },
            "type": {
                "enum": ["dcterms:BibliographicResource"]
            },
            # "bibClass": {
            #     "type": "string"
            # },
            # "bibClassLabel": {
            #     "type": "string"
            # },
            "title": {
                "anyOf": [
                    {
                        "type": "object",
                        "allowedClasses": ["Text"]
                    },
                    {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "allowedClasses": ["Text"]
                        }
                    }
                ]
            },
            "authorsList": {
                "anyOf": [
                    {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "allowedClasses": ["Agent"]
                        }
                    },
                    {
                        "type": "array",
                        "items": {
                            "type": "string",
                            "description": "_id of the Agent object"
                        }
                    }
                ]

            }
        },
        "required": ["title"]
    })

    # noinspection PyMethodOverriding
    def set_details(self, jsonClassLabel, title=None, source=None, selector=None, authors=None, **kwargs):
        super(BookPortion, self).set_details(source, selector, **kwargs)
        self.set_from_dict({
            "jsonClassLabel": jsonClassLabel,
            "authorsList": authors,
            "title": title
        })


class Page(SpecificResource):

    json_class = 'Page'

    schema = recursively_merge_json_schemas(SpecificResource.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["Page"]
            },
            "type": {
                "enum": ["dcterms:BibliographicResource"]
            },
            "purpose": {
                "enum": ["page"]
            }
        }
    })

    def __init__(self):
        super(Page, self).__init__()
        self.purpose = 'page'


updata_json_class_registry(sys.modules[__name__])
