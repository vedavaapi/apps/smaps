import sys

from . import Selector
from .. import CLASS_FIELD, DotDict
from .. import recursively_merge_json_schemas, merge_contexts, updata_json_class_registry


class FragmentSelector(Selector):
    """
    oa:FragmentSelector
    """
    # below is not property of data structure,
    # but just convenient spec registry,
    # hence, term not follows ontology.
    fragment_specs = DotDict({
        "HTML": "http://tools.ietf.org/rfc/rfc3236",
        "PDF": "http://tools.ietf.org/rfc/rfc3778",
        "TXT": "http://tools.ietf.org/rfc/rfc5147",
        "XML": "http://tools.ietf.org/rfc/rfc3023",
        "MEDIA": "http://www.w3.org/TR/media-frags/",
        "SVG": "http://www.w3.org/TR/SVG/"
    })

    json_class = 'FragmentSelector'

    context = merge_contexts(Selector.context, {
        "conformsTo": {
            "@id": "dcterms:conformsTo",
        },
        "value": {
            "@id": "rdf:value"
        }
    })

    schema = recursively_merge_json_schemas(Selector.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["FragmentSelector"]
            },
            "type": {
                "enum": ["oa:FragmentSelector"]
            },
            "conformsTo": {
                "type": "string",
            },
            "value": {
                "type": "string",
            }
        },
        "required": ["value"]
    })

    def set_details(self, value, conforms_to=None, refined_by=None, **kwargs):
        super(FragmentSelector, self).set_details(**kwargs)
        self.set_from_dict({
            "value": value,
            "conformsTo": conforms_to,
            "refinedBy": refined_by
        })

    @classmethod
    def media_fragment(cls, x, y, w, h):
        fragment_value = "xywh={},{},{},{}".format(x, y, w, h)
        conforms_to = FragmentSelector.fragment_specs.MEDIA
        fragment = cls.from_details(fragment_value, conforms_to=conforms_to)
        return fragment

    @classmethod
    def pdf_fragment(cls, page_no, vrx=None, vry=None, vrw=None, vrh=None):
        fragment_value = "page={}".format(page_no)
        if not (vrx is None or vry is None or vrw is None or vrh is None):
            view_rect = "viewrect={},{},{},{}".format(vrx, vry, vrw, vrh)
            fragment_value = "&".join((fragment_value, view_rect))
        conforms_to = FragmentSelector.fragment_specs.PDF
        fragment = cls.from_details(fragment_value, conforms_to=conforms_to)
        return fragment

    @classmethod
    def html_fragment(cls, named_section):
        fragment_value = "{}".format(named_section)
        conforms_to = FragmentSelector.fragment_specs.HTML
        fragment = cls.from_details(fragment_value, conforms_to=conforms_to)
        return fragment

    @classmethod
    def txt_fragment(cls, start, end):
        fragment_value = "char={},{}".format(start, end)
        conforms_to = FragmentSelector.fragment_specs.TXT
        fragment = cls.from_details(fragment_value, conforms_to=conforms_to)
        return fragment


class SvgSelector(Selector):

    json_class = 'SvgSelector'

    context = merge_contexts(Selector.context, {
        "value": {
            "@id": "rdf:value"
        }
    })

    schema = recursively_merge_json_schemas(Selector.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["SvgSelector"]
            },
            "type": {
                "enum": ["oa:SvgSelector"]
            },
            "value": {
                "type": "string",
            }
        },
        "required": ["value"]
    })

    def set_details(self, value, **kwargs):
        #  TODO svg processing have to be coded
        super(SvgSelector, self).set_details(**kwargs)
        self.set_from_dict({
            "value": value
        })


class TextQuoteSelector(Selector):

    json_class = 'TextQuoteSelector'

    context = merge_contexts(Selector.context, {
        "exact": {
            "@id": "oa:exact"
        },
        "prefix": {
            "@id": "oa:prefix"
        },
        "suffix": {
            "@id": "oa:suffix"
        }
    })

    schema = recursively_merge_json_schemas(Selector.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["TextQuoteSelector"]
            },
            "type": {
                "enum": ["oa:TextQuoteSelector"]
            },
            "exact": {
                "type": "string"
            },
            "prefix": {
                "type": "string"
            },
            "suffix": {
                "type": "string"
            }
        },
        "required": ["exact", "prefix", "suffix"]
    })

    def set_details(self, exact, prefix, suffix, **kwargs):
        super(TextQuoteSelector, self).set_details(**kwargs)
        self.set_from_dict({
            "exact": exact,
            "prefix": prefix,
            "suffix": suffix
        })


class TextPositionSelector(Selector):

    json_class = "TextPositionSelector"

    context = merge_contexts(Selector.context, {
        "start": {
            "@id": "oa:start",
            "@type": "xsd:nonNegativeInteger"
        },
        "end": {
            "@id": "oa:end",
            "@type": "xsd:nonNegativeInteger"
        },
        "cached": {
            "@id": "oa:cachedSource"
        }
    })

    schema = recursively_merge_json_schemas(Selector.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["TextPositionSelector"]
            },
            "type": {
                "enum": ["oa:TextPositionSelector"]
            },
            "start": {
                "type": "integer",
                "minimum": 0
            },
            "end": {
                "type": "integer",
                "minimum": 0
            },
            "cached": {
                "type": "string"
            }
        },
        "required": ["start", "end"]
    })

    def set_details(self, start, end, cached=None, **kwargs):
        super(TextPositionSelector, self).set_details(**kwargs)
        self.set_from_dict({
            "start": start,
            "end": end,
            "cached": cached
        })


class CssSelector(Selector):

    json_class = "CssSelector"

    context = merge_contexts(Selector.context, {
        "value": {
            "@id": "rdf:value"
        }
    })

    schema = recursively_merge_json_schemas(Selector.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["CssSelector"]
            },
            "type": {
                "enum": ["oa:CssSelector"]
            },
            "value": {
                "type": "string",
            }
        },
        "required": ["value"]
    })

    def set_details(self, value, **kwargs):
        super(CssSelector, self).set_details(**kwargs)
        self.set_from_dict({
            "value": value,
        })


class XpathSelector(Selector):

    json_class = 'XpathSelector'

    context = merge_contexts(Selector.context, {
        "value": {
            "@id": "rdf:value"
        }
    })

    schema = recursively_merge_json_schemas(Selector.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["XpathSelector"]
            },
            "type": {
                "enum": ["oa:XpathSelector"]
            },
            "value": {
                "type": "string",
            }
        },
        "required": ["value"]
    })

    def set_details(self, value, **kwargs):
        super(XpathSelector, self).set_details(**kwargs)
        self.set_from_dict({
            "value": value,
        })


class RangeSelector(Selector):

    json_class = 'RangeSelector'

    context = merge_contexts(Selector.context, {
        "startSelector": {
            "@id": "oa:hasStartSelector"
        },
        "endSelector": {
            "@id": "oa:hasEndSelector"
        }
    })

    schema = recursively_merge_json_schemas(Selector.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["RangeSelector"]
            },
            "type": {
                "enum": ["oa:RangeSelector"]
            },
            "startSelector": {
                "type": "object",
                "allowedClasses": ["Selector"]
            },
            "endSelector": {
                "type": "object",
                "allowedClasses": ["Selector"]
            }
        },
        "required": ["startSelector", "endSelector"]
    })

    def set_details(self, start_selector, end_selector, **kwargs):
        super(RangeSelector, self).set_details(**kwargs)
        self.set_from_dict({
            "startSelector": start_selector,
            "endSelector": end_selector
        })


class QualitativeSelector(Selector):

    json_class = 'QualitativeSelector'

    schema = recursively_merge_json_schemas(Selector.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["QualitativeSelector"]
            },
            "type": {
                "enum": ["oa:Selector"]
            }
        }
    })


updata_json_class_registry(sys.modules[__name__])
