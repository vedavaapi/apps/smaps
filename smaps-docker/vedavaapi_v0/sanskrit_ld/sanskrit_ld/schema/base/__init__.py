import sys
from datetime import datetime

from .. import JsonObject, CLASS_FIELD, updata_json_class_registry
from .. import merge_contexts, recursively_merge_json_schemas


class MetadataItem(JsonObject):
    """
    sld:MetadataItem
    """
    json_class = 'MetadataItem'

    context = merge_contexts(JsonObject.context, {
        "label": {
            "@id": "rdfs:label"
        },
        "value": {
            "@id": "rdf:value"
        },
    })

    schema = recursively_merge_json_schemas(JsonObject.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["MetadataItem"]
            },
            "type": {
                "enum": ["sld:MetadataItem"]
            },
            "label": {
                "type": "string"
            },
            "value": {

            }
        },
        "required": ["label", "value"]
    })

    @classmethod
    def from_details(cls, label, value):
        metadata_item = MetadataItem()
        metadata_item.set_from_dict({
            "label": label,
            "value": value
        })
        return metadata_item


# definitions of base concepts Resource, Selector, SpecificResource, Agent, Annotation.

class Resource(JsonObject):
    """
    rdfs:Resource
    """
    json_class = "Resource"  # in context, aliased to rdfs:Resource

    context = merge_contexts(MetadataItem.context, {
        "canonical": {
            "@id": "oa:canonical"
        },
        "via": {
            "@id": "oa:via"
        },
        "creator": {
            "@id": "dcterms:creator",
            "@type": "@id"
        },
        "created": {
            "@id": "dcterms:created",
            "@type": "xsd:dateTime"
        },
        "generator": {
            "@id": "as:generator",
            "@type": "@id"
        },
        "generated": {
            "@id": "dcterms:issued",
            "@type": "xsd:dateTime"
        },
        "modified": {
            "@id": "dcterms:modified",
            "@type": "xsd:dateTime"
        },
        "contributor": {
            "@id": "dcterms:contributor",
            "@type": "@id"
        },
        "purpose": {
            "@id": "oa:hasPurpose",
        },
        "metadata": {
            "@id": "sc:metadataLabels",
            "@type": "sld:MetadataItem"
        }
    })

    schema = recursively_merge_json_schemas(JsonObject.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["Resource"]
            },
            "type": {
                "enum": ["rdfs:Resource"]
            },
            "canonical": {
                "type": "string"
            },
            "via": {
                "type": "string"
            },
            "creator": {
                "anyOf": [
                    {
                        "type": "string"
                    },
                    {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    }
                ]
            },
            "created": {
                "type": "string",
                "format": "date-time"
            },
            "generator": {
                "type": "string",
            },
            "generated": {
                "type": "string",
                "format": "date-time"
            },
            "contributor": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            },
            "modified": {
                "type": "string",
                "format": "date-time"
            },
            "purpose": {
                "type": "string"
            },
            "metadata": {
                "type": "array",
                "items": {
                    "type": "object",
                    "allowedClasses": ["MetadataItem"]
                }
            }
        },
        "required": ["type"]
    })

    def __init__(self):
        super(Resource, self).__init__()
        self.update_time()

    def update_time(self):
        now = datetime.now()
        time_attr = 'modified' if hasattr(self, 'created') else 'created'
        setattr(self, time_attr, str(now))

    # noinspection PyIncorrectDocstring
    def set_details(
            self, creator=None, contributor=None, generator=None, generated=None,
            purpose=None, metadata=None, canonical=None, via=None, **kwargs):
        """

        :type contributor: list
        :type metadata: list
        """
        update_dict = {
            "creator": creator,
            "generator": generated,
            "generated": generated,
            "metadata": metadata,
            "canonical": canonical,
            "via": via,
            "purpose": purpose
        }
        if contributor is not None:
            update_dict['contributor'] = contributor
        elif creator is not None:
            update_dict['contributor'] = [creator]

        self.set_from_dict(update_dict)
        self.update_time()


class Selector(JsonObject):
    """
    oa:Selector
    """
    json_class = 'Selector'

    context = merge_contexts(JsonObject.context, {
        "refinedBy": {
            "@id": "oa:refinedBy"
        },
    })

    schema = recursively_merge_json_schemas(JsonObject.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["Selector"]
            },
            "type": {
                "enum": ["oa:Selector"]
            },
            "refinedBy": {
                "type": "object",
                "allowedClasses": ["Selector"]
            }
        },
    })


class SpecificResource(Resource):
    """
    oa:SpecificResource
    """
    json_class = 'SpecificResource'

    context = merge_contexts(JsonObject.context, Selector.context, {
        "source": {
            "@id": "oa:hasSource",
            "@type": "@id"
        },
        "selector": {
            "@id": "oa:hasSelector",
            "@type": "oa:Selector"
        }
    })

    schema = recursively_merge_json_schemas(Resource.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["SpecificResource"]
            },
            "type": {
                "enum": ["oa:SpecificResource"]
            },
            "source": {
                "type": "string",
            },
            "selector": {
                "type": "object",
                "allowedClasses": ["Selector", "SelectorChoice"]
            }
        },
        "required": [],
        "dependencies": {
            "selector": ["source"],
            "source": ["selector"]
        }
    })

    # noinspection PyMethodOverriding
    def set_details(self, source, selector, **kwargs):
        super(SpecificResource, self).set_details(**kwargs)
        self.set_from_dict({
            "source": source,
            "selector": selector
        })


class Text(Resource):
    """
    dctypes:Text
    """

    json_class = 'Text'

    context = merge_contexts(JsonObject.context, {
        "script": {
            "@id": "sld:scriptEncodingSchema",
            "@type": "sld:ScriptEncodingSchema"
        },
        "language": "dc:language",
        "chars": "cnt:chars"
    })

    schema = recursively_merge_json_schemas(Resource.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["Text"]
            },
            "type": {
                "enum": ["dctypes:Text"]
            },
            "chars": {
                "type": "string"
            },
            "language": {
                "type": "string"
            },
            "script": {
                "type": "string"
            }
        },
        "required": ["chars"]
    })

    # noinspection PyMethodOverriding
    def set_details(self, chars, language=None, script=None, **kwargs):
        super(Text, self).set_details(**kwargs)
        self.set_from_dict({
            "chars": chars,
            "language": language,
            "script": script
        })


class FileDescriptor(Resource):
    """
    sld:FileDescriptor
    bound to semantic change
    """
    json_class = 'FileDescriptor'

    context = merge_contexts(Resource.context, {
        "format": {
            "@id": "dcterms:format"
        },
        "path": {
            "@id": "sld:path"
        }
    })

    schema = recursively_merge_json_schemas(Resource.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["FileDescriptor"]
            },
            "type": {
                "enum": ["FileDescriptor"]
            },
            "format": {
                "type": "string"
            },
            "path": {
                "type": "string"
            }
        },
        "required": ["path"]
    })

    # noinspection PyMethodOverriding,PyShadowingBuiltins
    def set_details(self, path, format=None, **kwargs):
        super(FileDescriptor, self).set_details(**kwargs)
        self.set_from_dict({
            "path": path,
            "format": format
        })


class Agent(JsonObject):
    """
    dctypes:Agent; foaf:Agent
    """

    json_class = 'Agent'

    context = merge_contexts(JsonObject.context, Text.context, {
        "name": {
            "@id": "foaf:name",
            "@type": "dctypes:Text"
        },
        "agentClass": {
            "@id": "sld:agentClass"
        }
    })

    schema = recursively_merge_json_schemas(JsonObject.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["Agent"]
            },
            "type": {
                "enum": ["foaf:Agent"]
            },
            "name": {
                "anyOf": [
                    {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "allowedClasses": ["Text"]
                        },
                        "minItems": 1
                    },
                    {
                        "type": "object",
                        "allowedClasses": ["Text"]
                    }
                ],
            },
            "agentClass": {
                "type": "string",
                "enum": ["Software", "Person"]
            }
        }
    })

    def set_details(self, name=None, agent_class=None, canonical=None, via=None, **kwargs):
        super(Agent, self).set_details(**kwargs)
        update_dict = {
            "name": name,
            "canonical": canonical,
            "via": via,
            "agentClass": agent_class
        }
        self.set_from_dict(update_dict)


class Annotation(Resource):
    """
    oa:Annotation
    """
    json_class = 'Annotation'

    context = merge_contexts(Resource.context, {
        "body": {
            "@id": "oa:hasBody"
        },
        "bodyValue": {
            "@id": "oa:bodyvalue"
        },
        "target": {
            "@id": "oa:hasTarget"
        },
        "motivation": {
            "@id": "oa:motivatedBy",
            "@type": "oa:Motivation"
        }
    })

    schema = recursively_merge_json_schemas(Resource.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["Annotation"]
            },
            "type": {
                "enum": ["oa:Annotation"]
            },
            "motivation": {
                "anyOf": [
                    {
                        "type": "string"
                    },
                    {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    }
                ]
            },
            "body": {
                "anyOf": [
                    {
                        "type": "string"
                    },
                    {
                        "type": "object"
                    },
                    {
                        "type": "array",
                        "items": {
                            "type": "string"
                        },
                        "minItems": 1
                    },
                    {
                        "type": "array",
                        "items": {
                            "type": "object"
                        },
                        "minItems": 1
                    }
                ]
            },
            "bodyValue": {
                "type": "string"
            },
            "target": {
                "anyOf": [
                    {
                        "type": "string",
                        "description": "_id string"
                    },
                    {
                        "type": "array",
                        "items": {
                            "type": "string",
                            "description": "_id string"
                        }
                    }
                ]
            }
        },
        "required": ["target"]
    })

    # noinspection PyMethodOverriding
    def set_details(self, target, body=None, motivation=None, **kwargs):
        super(Annotation, self).set_details(**kwargs)
        self.set_from_dict({
            "target": target,
            "body": body,
            "motivation": motivation
        })


class Aggregate(Resource):

    """
    dctypes:Collection
    """

    json_class = 'Aggregate'

    context = merge_contexts(Resource.context, {
        "items": {
            "@id": "as:items"
        },
        "label": {
            "@id": "rdfs:label"
        }
    })

    schema = recursively_merge_json_schemas(Resource.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["Aggregate"]
            },
            "type": {
                "enum": ["dctypes:Collection"]
            },
            "items": {
                "type": "array",
                "items": {
                    "anyOf": [
                        {
                            "type": "string"
                        },
                        {
                            "type": "object"
                        }
                    ]
                }
            },
            "label": {
                "type": "string"
            }
        },
        "required": ["items"]
    })

    # noinspection PyMethodOverriding
    def set_details(self, items, label=None, **kwargs):
        super(Aggregate, self).set_details(**kwargs)
        self.set_from_dict({
            "items": items,
            "label": label
        })


class Choice(Resource):

    """
    oa:Choice
    """
    json_class = "Choice"

    context = merge_contexts(Resource.context, {
        "items": {
            "@id": "as:items"
        },
        "default": {
            "@id": "oa:default"
        },
        "label": {
            "@id": "rdfs:label"
        }
    })

    schema = recursively_merge_json_schemas(Resource.schema, {
        "type": "object",
        "definitions": {
            "choice": {
                "anyOf": [
                    {
                        "type": "string"
                    },
                    {
                        "type": "object"
                    }
                ]
            }
        },
        "properties": {
            CLASS_FIELD: {
                "enum": ["Choice"]
            },
            "type": {
                "enum": ["oa:Choice"]
            },
            "item": {
                "anyOf": [
                    {
                        "type": "array",
                        "items": {
                            "$ref": "#/definitions/choice"
                        }
                    },
                    {
                        "$ref": "#/definitions/choice"
                    }
                ]

            },
            "default": {
                "$ref": "#/definitions/choice"
            },
            "label": {
                "type": "string"
            }
        },
        "required": []
    })

    # noinspection PyMethodOverriding
    def set_details(self, items, default=None, **kwargs):
        super(Choice, self).set_details(**kwargs)
        self.set_from_dict({
            "item": items,
            "default": default
        })


class SelectorChoice(Choice):

    json_class = "SelectorChoice"

    schema = recursively_merge_json_schemas(Choice.schema, {
        "type": "object",
        "definitions": {
            "choice": {
                "anyOf": [
                    {
                        "type": "string"
                    },
                    {
                        "type": "object",
                        "allowedClasses": ["Selector"]
                    }
                ]
            }
        },
        "properties": {
            CLASS_FIELD: {
                "enum": ["SelectorChoice"]
            }
        }
    })


updata_json_class_registry(sys.modules[__name__])
