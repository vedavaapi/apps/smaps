import sys

from . import Agent
from .. import CLASS_FIELD, updata_json_class_registry
from .. import recursively_merge_json_schemas, merge_contexts


class Person(Agent):
    """
    foaf:Person
    """
    json_class = 'Person'

    context = merge_contexts(Agent.context, {
    })

    schema = recursively_merge_json_schemas(Agent.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["Person"]
            },
            "type": {
                "enum": ["foaf:Person"]
            },
        }
    })


class Organisation(Agent):
    """
    foaf:Organisation
    """
    json_class = 'Organisation'

    context = merge_contexts(Agent.context, {
    })

    schema = recursively_merge_json_schemas(Agent.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["Organisation"]
            },
            "type": {
                "enum": ["foaf:Organisation"]
            },
        }
    })


updata_json_class_registry(sys.modules[__name__])
