import sys

from six import string_types

from . import Annotation, Text, FileDescriptor

from .. import CLASS_FIELD, updata_json_class_registry
from .. import recursively_merge_json_schemas


class TextAnnotation(Annotation):

    """
    oa:Annotation;
    body type is Text
    """

    json_class = "TextAnnotation"

    schema = recursively_merge_json_schemas(Annotation.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["TextAnnotation"]
            },
            "type": {
                "enum": ["oa:Annotation"]
            },
            "body": {
                "anyOf": [
                    {
                        "type": "object",
                        "allowedClasses": ["Text"]
                    },
                    {
                        "type": "object",
                        "allowedClasses": ["Choice"]
                    },
                    {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "allowedClasses": ["Text"]
                        },
                        "minItems": 1
                    },
                    {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "allowedClasses": ["Choice"]
                        },
                        "minItems": 1
                    }
                ]
            },

        },
        "required": ["body"]
    })

    # noinspection PyMethodOverriding
    def set_details(self, text, target, **kwargs):
        body = None
        if isinstance(text, Text):
            body = text
        elif isinstance(text, string_types):
            text_obj = Text.from_details(text)
            body = text_obj
        elif isinstance(text, list):
            body = [
                obj if isinstance(obj, Text) else Text.from_details(obj)
                for obj in text
            ]
        super(TextAnnotation, self).set_details(target, body=body, **kwargs)


class FileAnnotation(Annotation):

    """
    oa:Annotation;
    motivation: oa:linking
    body type is FileDescriptor
    """

    json_class = "FileAnnotation"

    schema = recursively_merge_json_schemas(Annotation.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["FileAnnotation"]
            },
            "type": {
                "enum": ["sld:FileAnnotation"]
            },
            "motivation": {
                "enum": ["oa:linking"]
            },
            "body": {
                "anyOf": [
                    {
                        "type": "object",
                        "allowedClasses": ["FileDescriptor"]
                    },
                    {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "allowedClasses": ["FileDescriptor"]
                        },
                        "minItems": 1
                    }
                ]
            }
        }
    })

    # noinspection PyMethodOverriding
    def set_details(self, file, target, **kwargs):
        body = None
        if isinstance(file, FileDescriptor):
            body = file
        elif isinstance(file, string_types):
            fd_obj = FileDescriptor.from_details(file)
            body = fd_obj
        elif isinstance(file, list):
            body = [
                obj if isinstance(obj, FileDescriptor) else FileDescriptor.from_details(obj)
                for obj in file
            ]
        super(FileAnnotation, self).set_details(target, body=body, **kwargs)


updata_json_class_registry(sys.modules[__name__])
