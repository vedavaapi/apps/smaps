import sys

from ..base import Agent
from .. import CLASS_FIELD, JsonObject, updata_json_class_registry
from .. import recursively_merge_json_schemas


#  TODO should standardize permission ontology


class Permission(JsonObject):
    """
    sld:Permission
    """
    CREATE = "create"
    READ = "read"
    UPDATE = "update"
    DELETE = "delete"
    ADMIN = "admin"

    json_class = "Permission"

    schema = recursively_merge_json_schemas(JsonObject.schema, {
        "type": "object",
        "definitions": {
            "perms": {
                "type": "array",
                "items": {
                    "type": "string",
                    "enum": ["create", "read", "update", "delete", "admin"]
                }
            }
        },
        "properties": {
            CLASS_FIELD: {
                "enum": ["Permission"]
            },
            "type": {
                "enum": ["sld:Permission"]
            },
            "given": {
                "$ref": "#/definitions/perms"
            },
            "withdrawn": {
                "$ref": "#/definitions/perms"
            }
        },
    })

    def set_details(self, given, withdrawn):
        self.set_from_dict({
            "given": given,
            "withdrawn": withdrawn
        })


class UserPermissions(JsonObject):
    """
    sld:Permission
    """

    json_class = "UserPermissions"

    schema = recursively_merge_json_schemas(JsonObject.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["UserPermissions"]
            },
            "type": {
                "enum": ["owl:Thing"]
            },
            "global_scope": {
                "type": "object",
                "allowedClasses": ["Permission"]
            },
            "service_scopes": {
                "type": "object",
                "properties": {
                    CLASS_FIELD: {
                        "enum": ["WrapperObject"]
                    },
                    "type": {
                        "enum": ["owl:Thing"]
                    }
                },
                #  TODO type checking should support this too.
                "additionalProperties": {
                    "type": "object",
                    "properties": {
                        "global": {
                            "type": "object",
                            "allowedClasses": ["Permission"]
                        }
                    }
                }
            }
        }
    })

    def set_details(self, global_perm=None, service_perms=None):
        self.set_from_dict({
            "global_scope": global_perm,
            "service_scopes": service_perms
        })


def hash_password(plain_password):
    import bcrypt
    #   (Using bcrypt, the salt is saved into the hash itself)
    return bcrypt.hashpw(plain_password.encode(encoding='utf8'), bcrypt.gensalt()).decode(encoding='utf8')


class AuthenticationInfo(JsonObject):

    json_class = 'AuthenticationInfo'

    schema = recursively_merge_json_schemas(JsonObject.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["AuthenticationInfo"]
            },
            "type": {
                "enum": ["owl:Thing"]
            },
            "user_id": {
                "type": "string"
            },
            "provider": {
                "type": "string",
                "enum": ["google", "vedavaapi"]
            },
            "secret_bcrypt": {
                "type": "string",
                "description": "This should be hashed, and merits being stored in a database."
            },
            "secret_plain": {
                "type": "string",
                "description": "This should NEVER be set when stored in a database;"
                               "but is good for client-server transmission purposes."
            }
        }
    })

    VEDAVAAPI_AUTH = "vedavaapi"

    def __str__(self):
        # noinspection PyUnresolvedReferences
        return self.provider + "____" + self.user_id

    def check_password(self, plain_password):
        # Check hased password. Using bcrypt, the salt is saved into the hash itself
        import bcrypt
        # noinspection PyUnresolvedReferences
        return bcrypt.checkpw(plain_password.encode(encoding='utf8'), self.secret_bcrypt.encode(encoding='utf8'))

    def set_details(self, user_id, provider, secret_bcrypt=None):
        self.set_from_dict({
            "user_id": user_id,
            "provider": provider,
            "secret_bcrypt": secret_bcrypt
        })

    # noinspection PyUnresolvedReferences
    def set_bcrypt_password(self):
        if hasattr(self, "secret_plain") and self.secret_plain != "" and self.secret_plain is not None:
            # noinspection PyAttributeOutsideInit
            self.secret_bcrypt = hash_password(plain_password=self.secret_plain)
            delattr(self, "secret_plain")


class User(Agent):

    json_class = 'User'

    schema = recursively_merge_json_schemas(Agent.schema, {
        "type": "object",
        "properties": {
            CLASS_FIELD: {
                "enum": ["User"]
            },
            "type": {
                "enum": ["foaf:Agent"]
            },
            "authentication_infos": {
                "type": "array",
                "items": {
                    "type": "object",
                    "allowedClasses": ["AuthenticationInfo"]
                },
            },
            "permissions": {
                "type": "object",
                "allowedClasses": ["UserPermissions"]
            }
        }
    })

    # noinspection PyMethodOverriding
    def set_details(self, agent_class, auth_infos, permissions, **kwargs):
        self.set_from_dict({
            "agentClass": agent_class,
            "authentication_infos": auth_infos,
            "permissions": permissions
        })

    def check_permission(self, service, action):
        # noinspection PyUnresolvedReferences
        perms = self.permissions
        allowed = set()
        allowed.update(perms.global_scope.given)

        if hasattr(perms, 'service_scopes'):
            service_scopes = perms.service_scopes
            if hasattr(service_scopes, service):
                service_scope = service_scopes.service
                for perm in service_scope.withdrawn:
                    allowed.discard(perm)
                allowed.update(service_scope.given)
        return action in allowed or Permission.ADMIN in allowed


updata_json_class_registry(sys.modules[__name__])
