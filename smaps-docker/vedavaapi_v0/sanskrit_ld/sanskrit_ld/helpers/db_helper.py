from collections import OrderedDict

from pymongo.cursor import Cursor
from sanskrit_ld.helpers.validation_helper import validate
from sanskrit_ld.schema import JsonObject
from sanskrit_ld.schema.base.annotations import FileAnnotation
from vedavaapi.objectdb.mydb import MyDbCollection

from ..schema.users import User, Permission


class PermissionManager(object):

    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def has_persmission(self, user, action, obj=None):
        return True

    def check_permission(self, user, action, obj=None):
        has_perm = self.has_persmission(user, action, obj=obj)
        if not has_perm:
            raise PermissionError('user {} has no permission to {}'.format(str(user), action))


def read(colln, selector_doc, fields=None, return_generator=False):
    """

    :type colln: MyDbCollection
    :param colln:
    :param selector_doc:
    :param fields:
    :param return_generator:
    :return:
    """
    results_generator = colln.find(query=selector_doc, fields=fields, cursor=False)
    if return_generator:
        return results_generator
    return list(results_generator)


def read_by_id(colln, _id):
    """

    :type colln: MyDbCollection
    :param colln:
    :param _id:
    :return:
    """
    return colln.get(_id)


def read_and_do(colln, selector_doc, ops, fields=None, return_cursor=False, return_generator=False):
    """

    :type colln: MyDbCollection
    :type selector_doc: dict
    :type ops: OrderedDict
    :type return_cursor: bool
    :type return_generator: bool
    :param ops:
    :param fields:
    :return:
    """
    should_operate_on_cursor = True
    supported_ops = ['limit', 'skip', 'sort', 'count']
    for op in ops.keys():
        if op not in supported_ops:
            should_operate_on_cursor = False

    result_cursor = colln.find(query=selector_doc, fields=fields, cursor=True)  # type: Cursor
    resultant = result_cursor

    if should_operate_on_cursor:
        for op, args in ops.items():
            if not isinstance(resultant, Cursor):
                break
            op_method = resultant.__getattribute__(op)
            op_method(*args)

    if not isinstance(resultant, Cursor):
        # this implies, after operaions, we deduce some value, like count, etc.
        # so we just return that resultant value
        return resultant

    if return_cursor:
        return resultant
    if return_generator:
        return generator_for_cursor(colln, resultant)
    result_list = list(resultant)
    for item in result_list:
        colln.swizzle_with_id_fix(item)
    return result_list


def generator_for_cursor(colln, cursor):
    for item in cursor:
        colln.swizzle_with_id_fix(item)
        yield item


def update(colln, obj, user, permission_manager=None, validate_integrity=True):
    """

    :param validate_integrity:
    :type colln: MyDbCollection
    :type obj: JsonObject
    :type user: User
    :type permission_manager: PermissionManager
    :return:
    """
    if validate_integrity:
        validate(obj, colln)

    doc = obj.to_json_map()
    if '_id' in doc:
        # TODO clarification. should we update or replace? if to update, how to validate increment?
        if permission_manager is not None:
            permission_manager.check_permission(user, Permission.UPDATE, obj=obj)

        filter_dict = {'_id': doc['_id']}
        doc.pop('_id', None)
    else:
        if permission_manager is not None:
            permission_manager.check_permission(user, Permission.CREATE, obj=obj)
        filter_dict = doc

    updated_doc = colln.find_one_and_update(filter_dict, {'$set': doc}, upsert=True, return_doc='after')
    return updated_doc


def dependents(colln, obj):
    """

    :type colln: MyDbCollection
    :type obj: JsonObject
    :return:
    """
    # noinspection PyUnresolvedReferences,PyProtectedMember
    _id = obj._id
    spr_query_doc = {
        "source": _id
    }
    anno_query_doc = {
        "target": _id,
        "type": "oa:Annotation"
    }
    sprs = colln.find(query=spr_query_doc)
    spr_objects = [JsonObject.make_from_dict(spr) for spr in sprs]

    annos = colln.find(query=anno_query_doc)
    anno_objects = [JsonObject.make_from_dict(anno) for anno in annos]

    return spr_objects + anno_objects


def delete(colln, obj_or_id, user, permission_manager=None):
    """

    :type colln: MyDbCollection
    :type user: User
    :type permission_manager: PermissionManager
    :param obj_or_id:
    :return:
    """

    if isinstance(obj_or_id, JsonObject):
        obj = obj_or_id  # type: JsonObject
        # noinspection PyUnresolvedReferences,PyProtectedMember
        _id = obj_or_id._id
    else:
        obj = JsonObject.make_from_dict(colln.get(obj_or_id))
        if obj is None:
            return False, []
        _id = obj_or_id

    if isinstance(obj, FileAnnotation):
        return False, []

    if permission_manager is not None:
        has_delete_permission = permission_manager.has_persmission(user, Permission.DELETE, obj)
        if not has_delete_permission:
            return False, []

    # noinspection PyTypeChecker
    dependent_objs = dependents(colln, obj)

    descendant_approvals = []
    deleted_res_ids = []

    for d in dependent_objs:
        approval, deleted_descendant_ids = delete(colln, d, user, permission_manager=permission_manager)
        descendant_approvals.append(approval)
        deleted_res_ids.extend(deleted_descendant_ids)

    if False not in descendant_approvals:
        file_annos = files(colln, _id)
        for fanno in file_annos:
            colln.delete_item(fanno['_id'])
        # files will be deleted, as obj will be included in deleted_ids
        colln.delete_item(_id)
        deleted_res_ids.append(_id)
        return True, deleted_res_ids

    return False, deleted_res_ids


def delete_selection(colln, selector_doc, user, permission_manager=None):
    deleted_ids = []
    approvals = []
    matched_docs = read(colln, selector_doc)
    for doc in matched_docs:
        approval, deleted_descendant_ids = delete(colln, doc['_id'], user, permission_manager=permission_manager)
        deleted_ids.extend(deleted_descendant_ids)
        approvals.append(approval)

    return (False not in approvals), deleted_ids


def specific_resources(colln, resource_id, filter_doc=None, fields=None, return_generator=False):
    filter_doc = filter_doc or {}
    selector_doc = filter_doc.copy()
    std_spr_filter_doc = {
        "source": resource_id
    }
    selector_doc.update(std_spr_filter_doc)
    results = read(colln, selector_doc, fields=fields, return_generator=return_generator)
    return results


def annotations(colln, resource_id, filter_doc=None, fields=None, return_generator=False):
    filter_doc = filter_doc or {}
    selector_doc = filter_doc.copy()
    std_anno_filter_doc = {
        "target": resource_id,
        "type": "oa:Annotation"
    }
    selector_doc.update(std_anno_filter_doc)
    results = read(colln, selector_doc, fields=fields, return_generator=return_generator)
    return results


def files(colln, resource_id, filter_doc=None, fields=None, return_generator=False):
    filter_doc = filter_doc or {}
    selector_doc = filter_doc.copy()
    std_file_anno_filter_doc = {
        "target": resource_id,
        "type": "sld:FileAnnotation"
    }

    selector_doc.update(std_file_anno_filter_doc)
    results = read(colln, selector_doc, fields=fields, return_generator=return_generator)
    return results


def delete_specific_resources(colln, resource_id, user, filter_doc=None, permission_manager=None):
    filter_doc = filter_doc or {}
    selector_doc = filter_doc.copy()
    std_spr_filter_doc = {
        "source": resource_id
    }

    selector_doc.update(std_spr_filter_doc)
    deleted_all, deleted_ids = delete_selection(colln, selector_doc, user, permission_manager=permission_manager)
    return deleted_all, deleted_ids


def delete_annotations(colln, resource_id, user, filter_doc=None, permission_manager=None):
    filter_doc = filter_doc or {}
    selector_doc = filter_doc.copy()
    std_anno_filter_doc = {
        "target": resource_id,
        "type": "oa:Annotation"
    }

    selector_doc.update(std_anno_filter_doc)
    deleted_all, deleted_ids = delete_selection(colln, selector_doc, user, permission_manager=permission_manager)
    return deleted_all, deleted_ids
