from ..schema import JsonObject
from ..schema.base import Resource


class OrphanResourceError(BaseException):
    pass


def check_existence(_id, colln, obj_class=None):
    obj_json = colln.get(_id)
    if obj_json is None:
        return False
    if obj_json.get('jsonClass', None) == 'FileAnnotation':
        return False
    if obj_class is None:
        return True
    obj = JsonObject.make_from_dict(obj_json)
    if isinstance(obj, obj_class):
        return True
    return False


# noinspection PyUnusedLocal
def validate_json_object(obj, colln):
    pass


def validate_specific_resource(obj, colln):
    if not hasattr(obj, 'source'):
        return
    source_id = obj.source
    source_exists = check_existence(source_id, colln, obj_class=Resource)
    if not source_exists:
        raise OrphanResourceError('given SpecificResource object doesn\'t has valid source')


def validate_annotation(obj, colln):
    target_ids = obj.target
    if not isinstance(target_ids, list):
        target_ids = [target_ids]
    for target_id in target_ids:
        target_exists = check_existence(target_id, colln)
        if not target_exists:
            raise OrphanResourceError('target doesn\'t exist for given annotation')


validator_map = {
    "JsonObject": validate_json_object,
    "SpecificResource": validate_specific_resource,
    "Annotation": validate_annotation
}


def validator_function(json_class):
    if json_class.json_class in validator_map:
        return validator_map[json_class.json_class]
    base_class = json_class.__bases__[0]
    return validator_function(base_class)


def validate(obj, colln):
    json_class = obj.__class__
    validator_fn = validator_function(json_class)
    validator_fn(obj, colln)
