class Namespaces(object):

    """
    class wrapping standard namespaces.
    these should be used only as standard vocabulary, schema namespaces.
    """
    dc = "http://purl.org/dc/elements/1.1/"
    dcterms = "http://purl.org/dc/terms/"
    dctypes = "http://purl.org/dc/dcmitype/"
    foaf = "http://xmlns.com/foaf/0.1/"
    rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    rdfs = "http://www.w3.org/2000/01/rdf-schema#"
    oa = "http://www.w3.org/ns/oa#"
    sc = "http://iiif.io/api/presentation/2#"
    cnt = "http://www.w3.org/2011/content#"
    bibo = "http://purl.org/ontology/bibo/"

    skos = "http://www.w3.org/2004/02/skos/core#"
    xsd = "http://www.w3.org/2001/XMLSchema#"
    iana = "http://www.iana.org/assignments/relation/"
    owl = "http://www.w3.org/2002/07/owl#"
    ass = "http://www.w3.org/ns/activitystreams#"
    schema = "http://schema.org/"

    sld = "http://schema.vedavaapi.org/ns/sld/"


context = {
    # namespaces
    "dc": Namespaces.dc,
    "dcterms": Namespaces.dcterms,
    "dctypes": Namespaces.dctypes,
    "xsd": Namespaces.xsd,
    "foaf": Namespaces.foaf,
    "rdfs": Namespaces.rdfs,
    "rdf": Namespaces.rdf,
    "oa": Namespaces.oa,
    "sc": Namespaces.sc,
    "bibo": Namespaces.bibo,
    "as": Namespaces.ass,
    "sld": Namespaces.sld,

    # json-ld aliases
    "_id": "@id",
    "type": "@type",

    # other aliases
    "canonical": {
        "@id": "oa:canonical"
    },
    "via": {
        "@id": "oa:via"
    },
    "language": {
        "@id": "dc:language"
    },
    "metadata": {
        "@id": "sc:metadataLabels",
        "@type": "sld:MetadataItem"
    },
    "label": {
        "@id": "rdfs:label"
    },
    "value": {
        "@id": "rdf:value"
    },
    "creator": {
        "@id": "dcterms:creator",
        "@type": "@id"
    },
    "created": {
        "@id": "dcterms:created",
        "@type": "xsd:dateTime"
    },
    "generator": {
        "@id": "as:generator",
        "@type": "@id"
    },
    "generated": {
        "@id": "dcterms:issued",
        "@type": "xsd:dateTime"
    },
    "modified": {
        "@id": "dcterms:modified",
        "@type": "xsd:dateTime"
    },
    "contributor": {
        "@id": "dcterms:contributor",
        "@type": "@id"
    },
    "selector": {
        "@id": "oa:hasSelector",
        "@type": "oa:Selector"
    },
    "refinedBy": {
        "@id": "oa:refinedBy"
    },
    "state": {
        "@id": "oa:hasState"
    },
    "conformsTo": {
        "@id": "dcterms:conformsTo",
    },
    "source": {
        "@id": "oa:hasSource",
        "@type": "@id"
    },
    "purpose": {
        "@id": "oa:hasPurpose",
        "@type": "oa:Motivation"
    },
    "chars": {
        "@id": "cnt:chars"
    },
    "script": {
        "@id": "sld:scriptEncodingSchema",
        "@type": "sld:ScriptEncodingSchema"
    },
    "name": {
        "@id": "foaf:name",
        "@type": "dctypes:Text"
    },
    "body": {
        "@id": "oa:hasBody"
    },
    "bodyValue": {
        "@id": "oa:bodyvalue"
    },
    "target": {
        "@id": "oa:hasTarget"
    },
    "motivation": {
        "@id": "oa:motivatedBy",
        "@type": "oa:Motivation"
    },
    "rights": {
        "@id": "dcterms:rights"
    },
    "exact": {
        "@id": "oa:exact"
    },
    "prefix": {
        "@id": "oa:prefix"
    },
    "suffix": {
        "@id": "oa:suffix"
    },
    "start": {
        "@id": "oa:start",
        "@type": "xsd:nonNegativeInteger"
    },
    "end": {
        "@id": "oa:end",
        "@type": "xsd:nonNegativeInteger"
    },
    "cached": {
        "@id": "oa:cachedSource"
    },
    "startSelector": {
        "@id": "oa:hasStartSelector"
    },
    "endSelector": {
        "@id": "oa:hasEndSelector"
    },
    "format": {
        "@id": "dcterms:format"
    },
    "title": {
        "@id": "dcterms:title"
    },
    "bibClass": {
        "@id": "sld:bibliographicClass",
        "@type": "sld:BibliographicClass"
    },
    "bibClassLabel": {
        "@id": "sld:bibliographicClassLabel"
    },
    "authorsList": {
        "@id": "bibo:authorList",
        "@container": "@list"
    },
    "agentType": {
        "@id": "sld:agentType"
    },
    "items": {
        "@id": "as:items"
    },
    "default": {
        "@id": "oa:default"
    },

    "Thing": "owl:Thing",
    "Resource": "rdfs:Resource",
    "Selector": "oa:Selector",
    "State": "oa:State",
    "SpecificResource": "oa:SpecificResource",
    "Agent": "foaf:Agent",
    "Annotation": "oa:Annotation",

    "FragmentSelector": "oa:FragmentSelector",
    "Person": "foaf:Person",
    "Organisation": "foaf:Organisation",
}
