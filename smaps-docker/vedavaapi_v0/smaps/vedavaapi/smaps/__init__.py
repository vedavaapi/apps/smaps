import logging

from vedavaapi.common import VedavaapiService, ServiceRepo

logging.basicConfig(
    level=logging.INFO,
    format="%(levelname)s: %(asctime)s {%(filename)s:%(lineno)d}: %(message)s "
)


class SmapsRepo(ServiceRepo):

    def __init__(self, service, repo_name):
        super(SmapsRepo, self).__init__(service, repo_name)
        self.smaps_db_config = self.service.config['dbs']['smaps_db']
        self.smaps_db = self.db(self.smaps_db_config['name'])

        self.entities_colln = self.smaps_db.get_collection(self.smaps_db_config['collections']['entities'])

        self.relations_colln = self.smaps_db.get_collection(self.smaps_db_config['collections']['relations'])


class VedavaapiSmaps(VedavaapiService):
    instance = None

    dependency_services = ['store', 'gservices']
    svc_repo_class = SmapsRepo

    title = 'Vedavaapi Smaps'
    description = 'Shaastra maps'

    def __init__(self, registry, name, conf):
        super(VedavaapiSmaps, self).__init__(registry, name, conf)
        self.vvstore = self.registry.lookup('store')

    def entities_colln(self, repo_name):
        return self.get_repo(repo_name).entities_colln

    def relations_colln(self, repo_name):
        return self.get_repo(repo_name).relations_colln
