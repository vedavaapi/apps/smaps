from flask_restx import Namespace, Resource
from sanskrit_data.schema import common
from vedavaapi.common.api_common import get_repo, error_response

from .. import myservice
from ..import_helper import import_from_gsheet, check_if_modified, GranthaRowAdapter, VakyaRowAdapter, SambandhaRowAdapter

import_ns = Namespace('import api', description='api namespace with endpoints for importing', path='/import')


def gservices():
    repo_name = get_repo()
    return myservice().registry.lookup('gservices').services(repo_name)


@import_ns.route('/granthas/<string:id>')
class Grantha(Resource):
    post_parser = import_ns.parser()
    post_parser.add_argument('idType', location='args', type=str, default='Spreadsheet_id')
    post_parser.add_argument('overwrite', location='args', type=int, default=0)
    post_parser.add_argument('skip_errors', location='args', type=int, default=1)

    @import_ns.expect(post_parser, validate=True)
    def post(self, id):
        args = self.post_parser.parse_args()
        id_type = args.get('idType', 'Spreadsheet_id')
        if id_type not in ['Spreadsheet_id', 'Book_id']:
            return error_response(code=400, message='invalid idType parameter')

        grantha_selection_filter = {
            'jsonClass': 'Grantha',
            id_type: id
        }
        response, code = import_from_grantha(grantha_selection_filter, force=args.get('overwrite'), skip_errors=args.get('skip_errors'))
        return response, code


@import_ns.route('/granthas')
class Granthas(Resource):
    post_parser = import_ns.parser()
    post_parser.add_argument('import_data', location='args', type=int, default=0)
    post_parser.add_argument('overwrite', location='args', type=int, default=0)
    post_parser.add_argument('skip_errors', location='args', type=int, default=1)

    @import_ns.expect(post_parser, validate=True)
    def post(self):
        args = self.post_parser.parse_args()
        response = {}
        grantha_row_adapter = GranthaRowAdapter()
        catalog_import_response, catalog_import_status_code = import_from_gsheet(
            gservices(),
            myservice().config['catalog_spreadsheet_id'],
            myservice().config['catalog_sheet_title'],
            myservice().entities_colln(get_repo()),
            grantha_row_adapter
        )
        if 'error' in catalog_import_response:
            response['catalog_import_response'] = import_response(success=False, imported=False, code=424, inherited_error_response=catalog_import_response)
            return response, 424
        else:
            response['catalog_import_response'] = import_response()[0]

        if not bool(args.get('import_data', 0)):
            return response, 200

        all_grantha_jsons = list(myservice().entities_colln(get_repo()).find({
            'jsonClass': 'Grantha'
        }))
        if not all_grantha_jsons or not len(all_grantha_jsons):
            return import_response(success=False, imported=False, message='no granthas can be synced, with lack of information, please first try to import catalogue')
        individual_import_responses = {}

        for grantha_json in all_grantha_jsons:
            grantha_selection_filter = {
                'jsonClass': 'Grantha',
                'Book_id': grantha_json['Book_id']
            }
            grantha_import_response, grantha_import_status_code = import_from_grantha(grantha_selection_filter, force=args.get('overwrite'), skip_errors=args.get('skip_errors'))
            individual_import_responses[grantha_json['Book_id']] = {
                "response": grantha_import_response,
                "code": grantha_import_status_code
            }

        response['individual_import_responses'] = individual_import_responses

        return response, 200


def import_from_grantha(grantha_selection_filter, force=False, skip_errors=True):
    entities_colln = myservice().entities_colln(get_repo())
    relations_colln = myservice().relations_colln(get_repo())

    # 1. check grantha with given id defined in catalog.
    find_filter = grantha_selection_filter.copy()
    find_filter.update({'jsonClass': 'Grantha'})
    grantha_info = common.JsonObject.make_from_dict(
        entities_colln.find_one(find_filter)
    )
    if grantha_info is None:
        return error_response(
            message='given grantha seems not defined in catalog spreadsheet. if it is defined, please sync grantha catalog into db once.',
            code=404, success=False, imported=False
        )

    # 2. check if grantha spreadsheet is exposed_out or not
    if grantha_info.Status not in ['exposed_out']:
        return error_response(message='grantha with Book_id {} is not exposed_out.'.format(grantha_info.Book_id), code=403, imported=False, success=False)

    # 3. check if grantha spreadsheet is modified after last import.
    old_modified_time = grantha_info.modified_time if hasattr(grantha_info, 'modified_time') else '0'
    is_modified, new_modified_time = check_if_modified(gservices(), grantha_info.Spreadsheet_id, old_modified_time)
    # print(old_modified_time, new_modified_time)

    if not (is_modified or force):
        return import_response(imported=False,
                               message='importing skipped for grantha with Book_id:{} as it is not modified from time of last import'.format(
                                   grantha_info.Book_id))

    # 4. import Vakyas sheet rows into entities
    vakya_row_adapter = VakyaRowAdapter(grantha_info.Book_id, skip_errors=skip_errors)
    vakyas_import_response, vakyas_status_code = import_from_gsheet(
        gservices(),
        grantha_info.Spreadsheet_id,
        myservice().config['vakyas_sheet_title'],
        entities_colln,
        vakya_row_adapter
    )
    if 'error' in vakyas_import_response:
        return error_response(message='error in importing grantha', inherited_error_response=vakyas_import_response)

    # 5. update modified_time. as some part is imported from modified version
    grantha_info.modified_time = new_modified_time
    grantha_info.update_collection(entities_colln)

    # 6. now import Sambandhas sheet into relations collection
    sambandha_row_adapter = SambandhaRowAdapter(grantha_info.Book_id)
    sambandhas_import_response, sambandhas_status_code = import_from_gsheet(gservices(), grantha_info.Spreadsheet_id,
                                                                            myservice().config[
                                                                                'sambandhas_sheet_title'],
                                                                            relations_colln, sambandha_row_adapter)
    if 'error' in sambandhas_import_response:
        return error_response(message='error in importing grantha', inherited_error_response=sambandhas_import_response)

    return import_response()


def import_response(**kwargs):
    response = {
        'jsonClass': 'ImportStatus',
        'success': True,
        'imported': True,
        'code': 200
    }
    response.update(kwargs)
    return response, response['code']
