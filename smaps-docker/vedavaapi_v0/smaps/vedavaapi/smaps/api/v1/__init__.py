from flask import Blueprint
from flask_restx import Api

from .. import myservice


api_blueprint_v1 = Blueprint(myservice().name, __name__)


api = Api(
    app=api_blueprint_v1,
    version='1.0',
    title=myservice().title,
    prefix='/v1',
    description=myservice().description,
    doc='/v1'
)

from . import main_ns
from .import_ns import import_ns

api.add_namespace(import_ns)
