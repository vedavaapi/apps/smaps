# import sys
from logging import getLogger

from loris import constants
from loris.img_info import ImageInfo
# noinspection PyProtectedMember
from loris.resolver import _AbstractResolver, ResolverException

from .. import VedavaapiIiifImage

from . import IdentSwizzler

logger = getLogger(__name__)


class ServiceFSHelper(object):
    """
    sub class of this class should be implemented by vvservice which want to expose it's images through iiif
    """
    def __init__(self, repo_name):
        self.repo_name = repo_name

    def resolve_to_absolute_path(self, file_anno_id):
        pass


class VedavaapiFSResolver(_AbstractResolver):

    def __init__(self, config):
        super(VedavaapiFSResolver, self).__init__(config)

    # noinspection PyMethodMayBeStatic
    def raise_404_for_ident(self, ident):
        message = 'Source image not found for identifier: %s.' % (ident,)
        logger.warning(message)
        raise ResolverException(message)

    # noinspection PyMethodMayBeStatic
    def source_file_path(self, ident):
        ident_parts = IdentSwizzler.swizzle(ident)
        if ident_parts is None:
            return None
        repo_name, service_name, _id = ident_parts.values()

        service_obj = VedavaapiIiifImage.instance.registry.lookup(service_name)
        if not hasattr(service_obj, 'fs_helper'):
            return None
        file_path = service_obj.fs_helper(repo_name).resolve_to_absolute_path(_id)
        return file_path

    def is_resolvable(self, ident):
        return self.source_file_path(ident) is not None

    # noinspection PyMethodMayBeStatic
    def format_from_file_name(self, file_name):
        if file_name.rfind('.') != -1:
            extension = file_name.split('.')[-1]
            if len(extension) < 5:
                extension = extension.lower()
                return constants.EXTENSION_MAP.get(extension, extension)
        raise ResolverException(
            "Format could not be determined for %r." % file_name
        )

    def resolve(self, app, ident, base_uri):
        source_fp = self.source_file_path(ident)
        if source_fp is None:
            # same as is_resolvable()
            self.raise_404_for_ident(ident)

        format_ = self.format_from_file_name(source_fp.split('/')[-1])
        uri = self.fix_base_uri(base_uri)
        extra = self.get_extra_info(ident, source_fp)
        return ImageInfo(app, uri, source_fp, format_, extra)
