"""
additions, modifications to Loris to use with flask, and our framework
"""
try:
    # noinspection PyCompatibility
    from urllib.parse import unquote
except ImportError:  # Python 2
    # noinspection PyUnresolvedReferences
    from urllib import unquote

from collections import OrderedDict


class IdentSwizzler(object):

    @staticmethod
    def swizzle(ident):
        ident_split = unquote(ident).split('/', maxsplit=2)
        if len(ident_split) != 3:
            return None
        return OrderedDict([
            ('repo_name', ident_split[0]),
            ('service_name', ident_split[1]),
            ('_id', ident_split[2])
        ])

    @staticmethod
    def marshal(*args, **kwargs):
        if len(args) >= 3:
            try:
                return '/'.join(args)
            except TypeError:
                return None
        elif len(kwargs) >= 3:
            try:
                return '/'.join((kwargs['repo_name'], kwargs['service_name'], kwargs['_id']))
            except KeyError:
                return None
        else:
            return None
