from vedavaapi.common.api_common import get_repo, check_repo

from .. import VedavaapiIiifImage


def myservice():
    return VedavaapiIiifImage.instance


def get_loris_wrapper(repo_name=None):
    if repo_name is None:
        repo_name = get_repo()
    else:
        check_repo(repo_name)
    return myservice().loris_wrapper(repo_name)


from .v1 import api_blueprint_image
