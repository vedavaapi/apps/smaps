import os.path

import flask_restx
from flask import Blueprint

from .. import myservice


api_blueprint_image = Blueprint(myservice().name + '_image', __name__)


api = flask_restx.Api(
    app=api_blueprint_image,
    version='1.0',
    prefix='/v1',
    title=myservice().title + ' Image API',
    description="Vedavaapi IIIF Image API",
    doc='/v1'
)


def api_mount_url():
    return os.path.join(myservice().name.strip('/'), api.prefix.strip('/')) + '/'


from . import rest
