# import sys

import flask_restx
import flask
from flask import request

from vedavaapi.common.api_common import error_response, check_repo

from ...loris import IdentSwizzler
from .. import get_loris_wrapper
from . import api, api_mount_url


def loris_wrapper_for_ident(ident):
    ident_parts = IdentSwizzler.swizzle(ident)
    if ident_parts is None:
        return error_response(message='invalid identifier', code=404)
    repo_name = ident_parts['repo_name']
    check_repo(repo_name)
    loris_wrapper = get_loris_wrapper(repo_name=repo_name)
    return loris_wrapper


# noinspection PyMethodMayBeStatic,PyUnusedLocal,PyShadowingBuiltins
@api.route('/<repo_name>/<service_name>/<ident>/<region>/<size>/<rotation>/<quality>.<format>')
class Image(flask_restx.Resource):

    def get(self, repo_name, service_name, ident, region, size, rotation, quality, format):
        """
        IIIF image endpoint
        :param repo_name:
        :param service_name:
        :param ident: identifier
        :param region: region
        :param size: size
        :param rotation: rotation
        :param quality: quality
        :param format: format
        :return: Image response
        """
        loris_wrapper = get_loris_wrapper(repo_name)
        return loris_wrapper.image_response(request, api_mount_url())


# noinspection PyMethodMayBeStatic,PyUnusedLocal
@api.route('/<repo_name>/<service_name>/<ident>/info.json', endpoint='info')
class Info(flask_restx.Resource):

    def get(self, repo_name, service_name, ident):
        """
        IIIF info.json endpoint for an image with identifier <ident>
        :param repo_name:
        :param service_name:
        :param ident: identifier
        :return: info.json
        """
        loris_wrapper = get_loris_wrapper(repo_name)
        return loris_wrapper.info_response(request, api_mount_url())


# noinspection PyMethodMayBeStatic
@api.route('/<repo_name>/<service_name>/<ident>/')
class IdentRoot(flask_restx.Resource):

    def get(self, repo_name, service_name, ident):
        return flask.redirect(api.url_for(Info, repo_name=repo_name, service_name=service_name, ident=ident))
