from vedavaapi.common import VedavaapiService, ServiceRepo
from vedavaapi.client import VedavaapiClient


class IiifPresentationRepo(ServiceRepo):

    def __init__(self, service, repo_name):
        super(IiifPresentationRepo, self).__init__(service, repo_name)
        self.image_api_client = VedavaapiClient(self.service.image_api_conf()['url_root'], repo_name=self.repo_name)


class VedavaapiIiifPresentation(VedavaapiService):

    instance = None  # type: VedavaapiIiifPresentation

    dependency_services = ['store', 'ullekhanam']
    svc_repo_class = IiifPresentationRepo

    title = 'Vedavaapi IIIF presentation'
    description = 'Vedavaapi IIIF Presentation api'

    def __init__(self, registry, name, conf):
        super(VedavaapiIiifPresentation, self).__init__(registry, name, conf)

    def image_api_conf(self):
        return self.config['iiif_image_api']

    def si_registry(self, repo_name):
        return self.get_repo(repo_name).si_registry

    def image_api_client(self, repo_name):
        return self.get_repo(repo_name).image_api_client
