from vedavaapi.common.api_common import get_repo

from .. import VedavaapiIiifPresentation


def myservice():
    return VedavaapiIiifPresentation.instance


def get_image_api_client(repo_name=None):
    if repo_name is None:
        repo_name = get_repo()
    return myservice().image_api_client(repo_name)

def get_service_interface(repo_name, service_name):
    if repo_name is None:
        repo_name = get_repo()
    service_object = myservice().registry.lookup(service_name)
    if not hasattr(service_object, 'prezi_interface'):
        return None
    return service_object.prezi_interface(repo_name)


def image_api_conf():
    return myservice().image_api_conf()


from .v1 import api_blueprint_prezi
