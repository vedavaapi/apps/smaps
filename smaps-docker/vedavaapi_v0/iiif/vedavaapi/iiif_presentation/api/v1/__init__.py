import os.path

try:
    # noinspection PyCompatibility
    from urllib.parse import urljoin
except ImportError:  # Python 2
    # noinspection PyUnresolvedReferences,PyCompatibility
    from urlparse import urljoin

import flask_restx
from flask import Blueprint, request

from vedavaapi.common.api_common import check_repo

from .. import myservice, get_image_api_client, get_service_interface
from ...prezed.editor_factory import PresentationEditorFactory


api_blueprint_prezi = Blueprint(myservice().name + '_prezi', __name__)


api = flask_restx.Api(
    app=api_blueprint_prezi,
    version='1.0',
    prefix='/v1',
    title=myservice().title + ' Presentation API',
    description="Vedavaapi IIIF Presentation API",
    doc='/v1'
)


def api_mount_url():
    return os.path.join(myservice().name.strip('/'), api.prefix.strip('/')) + '/'


def get_editor_factory(repo_name, service_name):

    check_repo(repo_name)
    image_api_conf = myservice().image_api_conf()
    image_api_client = get_image_api_client(repo_name=repo_name)

    editor_factory = PresentationEditorFactory.from_details(
        get_service_interface(repo_name, service_name),
        image_api_client,
        request.url_root.rstrip('/') + '/' + api_mount_url(),
        None,  # TODO
        urljoin(image_api_conf['url_root'], image_api_conf['prefix'])
    )  # type: PresentationEditorFactory
    return editor_factory


from . import rest
