#  import sys

import flask_restx
from iiif_prezi.factory import StructuralError
from vedavaapi.common.api_common import error_response

from . import api
from . import get_editor_factory

from ...prezed.editor_factory import PresentationEditorFactory


# noinspection PyMethodMayBeStatic
@api.route('/<repo_name>/<service_name>/collection/<collection_id>.json')
class Collection(flask_restx.Resource):

    def get(self, repo_name, service_name, collection_id):
        editor_factory = get_editor_factory(repo_name, service_name)  # type: PresentationEditorFactory
        collection_editor = editor_factory.collection_editor
        collection = collection_editor.create(collection_id)
        if collection is None:
            return error_response(message='collection not found', code=404)
        return collection.toJSON()


# noinspection PyMethodMayBeStatic
@api.route('/<repo_name>/<service_name>/<object_id>/manifest.json')
class Manifest(flask_restx.Resource):

    def get(self, repo_name, service_name, object_id):
        editor_factory = get_editor_factory(repo_name, service_name)
        manifest_editor = editor_factory.manifest_editor
        manifest = manifest_editor.create(object_id)
        if manifest is None:
            return error_response(message='object not found', code=404)
        try:
            return manifest.toJSON()
        except StructuralError:
            return error_response(message='seems this book doesn\'t have any pages', code=404)


# noinspection PyMethodMayBeStatic
@api.route('/<repo_name>/<service_name>/<object_id>/sequence/<sequence_id>.json')
class Sequence(flask_restx.Resource):

    def get(self, repo_name, service_name, object_id, sequence_id):
        editor_factory = get_editor_factory(repo_name, service_name)
        sequence_editor = editor_factory.sequence_editor
        sequence = sequence_editor.create(object_id, sequence_id)
        if sequence is None:
            return error_response(message='object not found', code=404)
        try:
            return sequence.toJSON()
        except StructuralError:
            return error_response(message='seems no items in this sequence', code=404)


# noinspection PyMethodMayBeStatic
@api.route('/<repo_name>/<service_name>/<sequence_id>/canvas/<canvas_id>.json')
class Canvas(flask_restx.Resource):

    def get(self, repo_name, service_name, sequence_id, canvas_id):
        editor_factory = get_editor_factory(repo_name, service_name)
        canvas = editor_factory.canvas_editor.create(sequence_id, canvas_id)
        if canvas is None:
            return error_response(message='canvas not found', code=404)
        return canvas.toJSON()
