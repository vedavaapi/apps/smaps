from collections import namedtuple
from functools import reduce

try:
    # noinspection PyCompatibility
    from urllib.parse import unquote, quote_plus, urljoin, urlparse
except ImportError:  # Python 2
    # noinspection PyUnresolvedReferences
    from urllib import unquote, quote_plus
    # noinspection PyUnresolvedReferences,PyCompatibility
    from urlparse import urljoin, urlparse

from iiif_prezi.factory import *

from .resource_factory import CustomManifestFactory
from .sevices_helper import ServicePreziInterface


# noinspection PyMethodMayBeStatic,PyProtectedMember
class BaseMetadataObjectEditor(object):
    essential_meta_props = []
    essential_props = []
    resource_cls = BaseMetadataObject

    def __init__(self, editor_factory, uri_segment):
        """
        initializes all Editor Objects.
        takes editor_factory as param, so that every resource_editor can call editors of other resources.
        :param editor_factory:
        """
        self.editor_factory = editor_factory
        self.uri_segment = uri_segment
        self.service_interface = self.editor_factory.vv_service_interface  # type: ServicePreziInterface
        self.resource_factory = self.editor_factory.resource_factory

    def _new_base(self, *args, **kwargs):
        pass

    def full_uri_segment(self, *args, **kwargs):
        pass

    @classmethod
    def update_props(cls, resource, meta):
        for prop in meta:
            print(resource, prop, meta[prop])
            setattr(resource, prop, meta[prop])
        return resource


class CollectionEditor(BaseMetadataObjectEditor):
    essential_meta_props = ['label']
    essential_props = ['label']
    resource_cls = Manifest

    def full_uri_segment(self, collection_id):
        return '{}/{}'.format(
            self.uri_segment,
            collection_id
        )

    def _new_base(self, collection_id, meta, container=None):
        meta = meta.copy()
        for prop in self.__class__.essential_meta_props:
            if prop not in meta:
                return None
        ident = self.full_uri_segment(collection_id)
        collection = (container or self.resource_factory).collection(ident=ident, label=meta.pop('label'))
        self.update_props(collection, meta)
        return collection

    def register_manifests(self, collection, object_ids):
        for object_id in object_ids:
            object_details = self.service_interface.object_details(object_id)
            if object_details is None:
                continue
            # noinspection PyProtectedMember
            self.editor_factory.manifest_editor._new_base(
                object_id, object_details.get('meta', {}), container=collection
            )

    def register_collections(self, collection, sub_collection_ids):
        for sub_collection_id in sub_collection_ids:
            self._new_base(sub_collection_id, {"label": sub_collection_id}, container=collection)

    def from_details(self, collection_id, meta, object_ids, sub_collection_ids=None):
        collection = self._new_base(collection_id, meta)
        if object_ids is not None:
            self.register_manifests(collection, object_ids)
        if sub_collection_ids is not None:
            self.register_collections(collection, sub_collection_ids)
        return collection

    def create(self, collection_id):
        collection_details = self.service_interface.collection_details(collection_id)
        if collection_details is None:
            return None

        return self.from_details(
            collection_id, collection_details.get('meta', None), collection_details.get('object_ids', [])
        )


# noinspection PyMethodMayBeStatic
class ManifestEditor(BaseMetadataObjectEditor):
    essential_meta_props = ['label']
    essential_props = ['label']
    resource_cls = Manifest

    def full_uri_segment(self, object_id):
        return '{}/{}'.format(
            object_id,
            self.uri_segment
        )

    def _new_base(self, object_id, meta, container=None):
        meta = meta.copy()
        for prop in self.__class__.essential_meta_props:
            if prop not in meta:
                return None
        ident = self.full_uri_segment(object_id)
        manifest = (container or self.resource_factory).manifest(ident=ident, label=meta.pop('label'))
        self.update_props(manifest, meta)
        return manifest

    def add_sequence(self, manifest, sequence):
        manifest.add_sequence(sequence)

    # noinspection PyUnusedLocal
    def from_details(self, object_id, meta, default_sequence_id, sequence_ids=None):
        manifest = self._new_base(object_id, meta)
        default_sequence = self.editor_factory.sequence_editor.create(object_id, default_sequence_id)
        self.add_sequence(manifest, default_sequence)
        #  TODO for remaining sequences, add their IRIs in list
        return manifest

    def create(self, object_id):
        object_details = self.service_interface.object_details(object_id)
        if object_details is None:
            return None
        return self.from_details(
            object_id,
            object_details.get('meta', {}),
            object_details.get('default_sequence_id', None),
            object_details.get('sequence_ids', []))


# noinspection PyMethodMayBeStatic
class SequenceEditor(BaseMetadataObjectEditor):
    essential_meta_props = []
    essential_props = []
    resource_cls = Sequence

    def full_uri_segment(self, object_id, sequence_id):
        return '{}/{}/{}'.format(
            object_id,
            self.uri_segment,
            sequence_id
        )

    def _new_base(self, object_id, sequence_id, meta, container=None):
        meta = meta.copy()
        for prop in self.__class__.essential_meta_props:
            if prop not in meta:
                return None
        ident = self.full_uri_segment(object_id, sequence_id)
        sequence = (container or self.resource_factory).sequence(ident=ident)
        self.update_props(sequence, meta)
        return sequence

    def add_canvas(self, sequence, canvas):
        sequence.add_canvas(canvas)

    def add_canvases(self, sequence, object_id, sequence_id, canvas_ids):
        for canvas_id in canvas_ids:
            canvas = self.editor_factory.canvas_editor.create(sequence_id, canvas_id)
            if canvas is None:
                continue
            self.add_canvas(sequence, canvas)

    def from_details(self, object_id, sequence_id, meta, canvas_ids):
        sequence = self._new_base(object_id, sequence_id, meta)
        self.add_canvases(sequence, object_id, sequence_id, canvas_ids)
        return sequence

    def create(self, object_id, sequence_id):
        sequence_details = self.service_interface.sequence_details(object_id, sequence_id)
        if sequence_details is None:
            return None
        return self.from_details(
            object_id, sequence_id, sequence_details.get('meta', {}), sequence_details.get('canvas_ids', [])
        )


# noinspection PyMethodMayBeStatic
class CanvasEditor(BaseMetadataObjectEditor):
    essential_meta_props = ['label']
    essential_props = ['label', 'height', 'width']
    resource_cls = Canvas

    def full_uri_segment(self, sequence_id, canvas_id):
        return '{}/{}/{}'.format(
            sequence_id,
            self.uri_segment,
            canvas_id
        )

    def _new_base(self, sequence_id, canvas_id, meta, container=None):
        meta = meta.copy()
        for prop in self.__class__.essential_meta_props:
            if prop not in meta:
                return None
        ident = self.full_uri_segment(sequence_id, canvas_id)
        canvas = (container or self.resource_factory).canvas(
            ident=ident, label=meta.pop('label'), mdhash=meta.pop('mdhash', {})
        )
        self.update_props(canvas, meta)
        return canvas

    def set_full_image_annotation(self, canvas, image_id):
        iiif_image_ident = '{}/{}/{}'.format(
            self.service_interface.repo_name,
            self.service_interface.service_name,
            image_id
        )
        # self.create_image_annotation(canvas, iiif_image_ident)
        print('set_full_image_called', canvas, image_id)
        return canvas.set_image_annotation(iiif_image_ident, iiif=True)

    # deprecated
    def create_image_annotation(self, canvas, iiif_image_ident):
        full_url = self.resource_factory.default_base_image_uri.rstrip('/') + '/' + iiif_image_ident + '/info.json'
        path = urlparse(full_url).path
        request_cls = namedtuple('Request', ["url", "path"])
        request = request_cls(full_url, path)

        from .. import VedavaapiIiifPresentation
        service = VedavaapiIiifPresentation.instance
        loris_wrapper = service.registry.lookup('iiif_image').loris_wrapper(self.service_interface.repo_name)
        # noinspection PyProtectedMember
        info = loris_wrapper.loris._get_info(
            iiif_image_ident, request, self.resource_factory.default_base_image_uri+'/'+iiif_image_ident)[0]

        anno = canvas.annotation()
        img = anno.image(iiif_image_ident, iiif=True)
        img.width = info.width
        img.height = info.height
        canvas.height = img.height
        canvas.width = img.width

    # noinspection PyUnusedLocal
    def from_details(self, sequence_id, canvas_id, meta, image_id=None, image_ids=None, dimensions=None):
        canvas = self._new_base(sequence_id, canvas_id, meta)
        if image_id is not None:
            print('calling sfa', file=sys.stderr)
            self.set_full_image_annotation(canvas, image_id)
        return canvas

    def create(self, sequence_id, canvas_id):
        canvas_details = self.service_interface.canvas_details(sequence_id, canvas_id)
        if canvas_details is None:
            return None
        return self.from_details(
            sequence_id, canvas_id, canvas_details.get('meta', {}), image_id=canvas_details['image_id']
        )


class LayerEditor(BaseMetadataObjectEditor):
    essential_meta_props = ['label']
    essential_props = ['label']
    resource_cls = Layer

    def full_uri_segment(self, object_id, layer_id):
        return '{}/{}/{}'.format(
            object_id,
            self.uri_segment,
            layer_id
        )

    # TODO


EditorDefinition = namedtuple('EditorDefinition', ('editor_class', 'uri_segment'))


# noinspection PyMethodMayBeStatic
class PresentationEditorFactory(object):
    editor_definitions = {
        'collection': EditorDefinition(CollectionEditor, 'collection'),
        'manifest': EditorDefinition(ManifestEditor, 'manifest'),
        'sequence': EditorDefinition(SequenceEditor, 'sequence'),
        'canvas': EditorDefinition(CanvasEditor, 'canvas'),
        # 'annotation_list': EditorDefinition(AnnotationListEditor, 'list'),
        # 'annotation': EditorDefinition(AnnotationEditor, 'annotation'),
        'layer': EditorDefinition(LayerEditor, 'layer')
    }

    def __init__(self, vv_service_interface, resource_factory):
        self.vv_service_interface = vv_service_interface
        self.resource_factory = resource_factory
        self._ed_instance_registry = {}

    # noinspection PyUnusedLocal
    @classmethod
    def from_details(
            cls, vv_service_interface, image_api_client,
            base_prezi_uri, base_prezi_dir,
            base_image_uri, iiif_image_info=(2.0, 2)):

        resource_factory = CustomManifestFactory(image_api_client)

        full_base_prezi_uri = reduce(lambda base, segm: urljoin(base.rstrip('/') + '/', segm.lstrip('/')), [
            base_prezi_uri,
            vv_service_interface.repo_name,
            vv_service_interface.service_name,
        ])
        resource_factory.set_base_prezi_uri(full_base_prezi_uri)

        resource_factory.set_base_image_uri(base_image_uri)
        resource_factory.set_iiif_image_info(*iiif_image_info)
        return cls(vv_service_interface, resource_factory)

    def resource_uri(self, uri_segment):
        return urljoin(
            self.resource_factory.prezi_base,
            uri_segment.lstrip('/') + '.json'
        )

    def _editor_instance(self, resource_type, force=False):
        if resource_type not in self.editor_definitions:
            return None
        instance = self._ed_instance_registry.get(resource_type, None)
        if (instance is None) or force:
            editor_definition = self.editor_definitions[resource_type]
            self._ed_instance_registry[resource_type] = editor_definition.editor_class(
                self, editor_definition.uri_segment
            )
            instance = self._ed_instance_registry.get(resource_type)
        return instance

    @property
    def collection_editor(self):
        return self._editor_instance('collection')  # type: CollectionEditor

    @property
    def manifest_editor(self):
        return self._editor_instance('manifest')  # type: ManifestEditor

    @property
    def sequence_editor(self):
        return self._editor_instance('sequence')  # type: SequenceEditor

    @property
    def canvas_editor(self):
        return self._editor_instance('canvas')  # type: CanvasEditor

    '''
    @property
    def annotation_list_editor(self):
        return self._editor_instance('annotation_list')

    @property
    def annotation_editor(self):
        return self._editor_instance('annotation')
    '''

    @property
    def layer_editor(self):
        return self._editor_instance('layer')  # type: LayerEditor


# following is experimental registry for caching factory instances if we want. not using this yet.
class EditorFactoryRegistryForRepo(object):

    def __init__(self, repo_name, si_registry, image_api_client, base_prezi_uri, base_prezi_dir,
                 base_image_uri, iiif_image_info=(2.0, 2)):
        self.repo_name = repo_name
        self.si_registry = si_registry
        self.image_api_client = image_api_client
        self.base_prezi_uri = base_prezi_uri
        self.base_prezi_dir = base_prezi_dir
        self.base_image_uri = base_image_uri
        self.iiif_image_info = iiif_image_info
        self._editors_registry = {}

    def editor_factory(self, vv_service_name):
        if vv_service_name in self._editors_registry:
            return self._editors_registry[vv_service_name]
        new_editor = PresentationEditorFactory.from_details(
            self.si_registry.si_instance(vv_service_name),
            self.image_api_client,
            self.base_prezi_uri,
            self.base_prezi_dir,
            self.base_image_uri,
            self.iiif_image_info
        )
        self._editors_registry[vv_service_name] = new_editor
        return new_editor
