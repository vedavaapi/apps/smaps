import re

from sanskrit_data.schema.common import JsonObject


class ServicePreziInterface(object):

    service_name = 'service'

    def __init__(self, repo_name):
        self.repo_name = repo_name

    def collection_details(self, collection_id):
        pass

    def object_details(self, object_id):
        # meta, sequences
        pass

    def sequence_details(self, object_id, sequence_id):
        # meta, canvases
        pass

    def canvas_details(self, sequence_id, canvas_id):
        # meta, image or (images and dimensions)
        # TODO optimize url
        pass
