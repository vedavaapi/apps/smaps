import sys

import pymongo
# from pymongo.database import Database
# noinspection PyPackageRequirements
from bson import ObjectId
from pymongo.cursor import Cursor

from vedavaapi.objectdb import DotDict
from .mydb import *


# noinspection PyProtectedMember
class MongoDbCollection(DBMSCollection):

    def __init__(self, mongo_database, collection_name):
        super(MongoDbCollection, self).__init__(mongo_database, collection_name)
        self._mongo_collection = self.dbms_db._mongo_collection(collection_name)

    def swizzle_with_id_fix(self, doc):
        if doc is not None and "_id" in doc:
            doc["_id"] = str(doc["_id"])
        return doc
        
    def all(self):
        return self._mongo_collection.find()

    def count(self):
        return self._mongo_collection.count()

    def drop(self):
        return self._mongo_collection.drop()
        
    def find_one(self, query):
        result = self._mongo_collection.find_one(query)
        self.swizzle_with_id_fix(result)
        return result

    def find(self, query, projection=None, cursor=False):
        results = self._mongo_collection.find(query, projection)  # type: Cursor
        if cursor:
            return results
        else:
            return self.find_generator(results)

    def find_generator(self, results):
        for result in results:
            self.swizzle_with_id_fix(result)
            yield result

    def insert_one(self, item):
        mres = self._mongo_collection.insert_one(item)
        result = DotDict({
            'inserted_id': str(mres.inserted_id)
        })
        return result

    def insert_many(self, items):
        mres = self._mongo_collection.insert_many(items)
        result = DotDict({
            'inserted_ids': [str(inserted_id) for inserted_id in mres.inserted_ids]
        })
        return result
        
    def update_one(self, query, update_doc, upsert=False):
        mres = self._mongo_collection.update_one(query, update_doc, upsert=upsert)
        result = DotDict({
            'n': mres.modified_count
        })
        return result

    def update_many(self, query, update_doc, upsert=False):
        mres = self._mongo_collection.update_many(query, update_doc, upsert=upsert)
        result = DotDict({
            'n': mres.modified_count
        })
        return result

    def delete_one(self, query):
        mres = self._mongo_collection.delete_one(query)
        result = DotDict({
            'n': mres.deleted_count
        })
        return result

    def delete_many(self, query):
        mres = self._mongo_collection.delete_many(query)
        result = DotDict({
            'n': mres.deleted_count
        })
        return result

    def find_one_and_update(self, query, update_doc, upsert=False, return_doc='after'):
        mongo_return_doc = {
            'before': pymongo.collection.ReturnDocument.BEFORE,
            'after': pymongo.collection.ReturnDocument.AFTER
        }.get(return_doc, pymongo.collection.ReturnDocument.AFTER)
        doc = self._mongo_collection.find_one_and_update(
            query,
            update_doc,
            upsert=upsert,
            return_document=mongo_return_doc
        )
        self.swizzle_with_id_fix(doc)
        return doc

    def object_id(self, id_obj):
        import six
        if isinstance(id_obj, six.string_types):
            return ObjectId(id_obj)
        else:
            return id_obj

    def create_index(self, keys_dict, index_name):
        self._mongo_collection.create_index(list(keys_dict.items()), name=index_name, background=True)


# noinspection PyProtectedMember
class MongoDbDatabase(DBMSDatabase):

    def __init__(self, mongo_client, db_name):
        super(MongoDbDatabase, self).__init__(mongo_client, db_name)
        self._mongo_database = self.dbms_client._mongo_database(db_name)

    def _mongo_collection(self, collection_name):
        return self._mongo_database.get_collection(collection_name)

    def list_collection_names(self):
        return self._mongo_database.list_collection_names()

    def get_collection(self, collection_name):
        return MongoDbCollection(self, collection_name)

    def drop_collection(self, collection_name):
        self._mongo_database.drop_collection()


class MongoDbClient(DBMSClient):

    def __init__(self, host_uri, port=None):
        port = 27017 if port is None else port
        super(MongoDbClient, self).__init__(host_uri, port=port)
        self._mongo_client = pymongo.MongoClient(host_uri, port=port)

    def _mongo_database(self, db_name):
        return self._mongo_client.get_database(db_name)

    def get_database(self, db_name):
        return MongoDbDatabase(self, db_name)

    def list_database_names(self):
        return self._mongo_client.list_database_names()

    def drop_database(self, db_name):
        return self._mongo_client.drop_database(db_name)

    def close(self):
        self._mongo_client.close()
