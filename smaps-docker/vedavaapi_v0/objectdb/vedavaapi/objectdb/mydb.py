import re
import csv

# import json
# from operator import itemgetter, attrgetter, methodcaller
            
def table2json(t):
    return [dict(zip(t['fields'], valrow)) for valrow in t['values']]

# Base class to encapsulate a MYDB Table
class DBMSCollection:

    def __init__(self, dbms_db, collection_name):
        self.dbms_db = dbms_db
        self.name = collection_name
        # subclass should override it along with calling super.

    def all(self):
        raise TypeError("No method definition available: DBMSCollection.all.")
        
    def count(self):
        raise TypeError("No method definition available: DBMSCollection.count.")
        
    def drop(self):
        raise TypeError("No method definition available: DBMSCollection.reset.")
        
    def find_one(self, query):
        raise TypeError("No method definition available: DBMSCollection.find_one.")

    def find(self, query, projection=None, cursor=False):
        raise TypeError("No method definition available: DBMSCollection.find")
        
    def insert_one(self, item):
        raise TypeError("No method definition available: DBMSCollection.insert_one.")

    def insert_many(self, items):
        raise TypeError("No method definition available: DBMSCollection.insert_many.")
        
    def update_one(self, query, mod_spec, upsert=False):
        raise TypeError("No method definition available: DBMSCollection.update_one.")

    def update_many(self, query, mod_spec, upsert=False):
        raise TypeError("No method definition available: DBMSCollection.update_many.")

    def delete_one(self, query):
        raise TypeError("No method definition available: DBMSCollection.delete_one.")

    def delete_many(self, query):
        raise TypeError("No method definition available: DBMSCollection.delete_many.")

    def find_one_and_update(self, query, update_doc, upsert=False, return_doc='after'):
        raise TypeError("No method definition available: DBMSCollection.find_one_and_update.")

    def object_id(self, id_str):
        raise TypeError("No method definition available: DBMSCollection.object_id.")

    def swizzle_with_id_fix(self, doc):
        raise TypeError("not implemented")

    def create_index(self, keys_dict, index_name):
        raise TypeError("No method definition available: DBMSCollection.create_index.")


class DBMSDatabase(object):

    def __init__(self, dbms_client, db_name):
        self.dbms_client = dbms_client
        self.name = db_name
        # should override it according to each dbms, after calling super

    def __getitem__(self, name):
        return self.get_collection(name)

    def list_collection_names(self):
        pass

    def get_collection(self, collection_name):
        pass

    def drop_collection(self, collection_name):
        pass


class DBMSClient(object):

    def __init__(self, host_uri, port=None):
        self.host_uri = host_uri
        self.port = port
        # should override according to each DBMS after calling super

    def list_database_names(self):
        pass

    def get_database(self, db_name):
        pass

    def drop_database(self, db_name):
        pass

    def close(self):
        pass

        
# MYDB collection with import/export functionality
class MyDbCollection:
    def __init__(self, dbms_collection, cache=False):
        self.dbms_collection = dbms_collection  # type: DBMSCollection
        self.name = self.dbms_collection.name
        self.cache = cache
        self.local = None
        self.schema = None
        if cache:
            self.slurp()

    def marshal_with_object_id_fix(self, doc):
        if doc is None:
            return None
        if '_id' in doc:
            doc['_id'] = self.dbms_collection.object_id(doc['_id'])
        return doc

    def swizzle_with_id_fix(self, doc):
        self.dbms_collection.swizzle_with_id_fix(doc)
        return doc

    def slurp(self):
        self.local = {}
        for o in self.dbms_collection.find():
            o['_id'] = str(o['_id'])
            self.local[o['_id']] = o

        return self.local

    def all(self):
        if not self.local:
            self.slurp()
        return self.local

    def count(self):
        return self.dbms_collection.count()

    def to_json(self):
        return {self.name: self.slurp()}

    def from_json(self, data):
        self.dbms_collection.drop()
        if data:
            # Save the first row contents as schema table 
            # after emptying its values.
            self.schema = data[0]
            for k in self.schema.keys():
                self.schema[k] = ''
        try:
            for d in data:
                self.insert_one(d)
        except Exception as e:
            print("Error inserting into " + self.name + ": ", e)
        if self.cache:
            self.slurp()

    def from_csv(self, fname, primarykey=None):
        table = {'name': self.name}
        with open(fname) as f:
            gothdr = False
            idx = {}
            primaryidx = None
            for row in csv.reader(f):
                if not gothdr:
                    gothdr = True
                    row[0] = row[0].lstrip('#')
                    idx = dict(zip(row, range(len(row))))
                    table['fields'] = []
                    table['fields'].extend(row)
                    if primarykey:
                        table['fields'].append('_id')
                        primaryidx = idx[primarykey]

                    table['values'] = []
                    continue
                if row[0].startswith('#'):
                    continue
                # print [re.split(r'\s*,\s*', r) for r in row if ',' in r]
                row = list(map(lambda x: re.split(r',\s*', x) if ',' in x else x, row))
                values = row
                # print row
                if primarykey:
                    values.append(values[primaryidx])
                table['values'].append(values)
            self.from_json(table2json(table))

    '''
    def __repr__(self):
        return json.dumps(self.to_json())
    '''

    def oid(self, item_id):
        try:
            return {'_id': self.dbms_collection.object_id(item_id)}
        except Exception as e:
            print("Error: invalid object id ", e)
            return None

    def get(self, item_id):
        res = self.local[item_id] if self.cache else None
        if not res:
            query = self.oid(item_id)
            if not query:
                return None
            res = self.dbms_collection.find_one(query)
        return res

    def find_one(self, query):
        self.marshal_with_object_id_fix(query)
        res = self.dbms_collection.find_one(query)
        return res

    def find(self, query=None, fields=None, cursor=False):
        if query is None:
            query = {}
        if fields is None:
            fields = []
        self.marshal_with_object_id_fix(query)

        if len(fields) > 0:
            f = dict((k, 1) for k in fields)
            return self.dbms_collection.find(query, projection=f, cursor=cursor)
        else:
            return self.dbms_collection.find(query, cursor=cursor)

    def find_raw(self, query=None, projection=None, cursor=False):
        if query is None:
            query = {}
        self.marshal_with_object_id_fix(query)
        return self.dbms_collection.find(query, projection=projection, cursor=cursor)

    def insert_one(self, item):
        # print "Inserting",item
        try:
            result = self.dbms_collection.insert_one(item)
        except Exception as e:
            print("Error inserting into " + self.name + ": ", e)
            return None
        return result.inserted_id

    def insert_many(self, items):
        try:
            result = self.dbms_collection.insert_many(items)
        except Exception as e:
            print('Error inserting into {}'.format(self.name), e)
            return None
        return result.inserted_ids

    def update_item(self, item_id, fields):
        query = self.oid(item_id)
        return self.update_one(query, fields) > 0

    def update_one(self, query, update_doc, upsert=False):
        self.marshal_with_object_id_fix(query)
        self.marshal_with_object_id_fix(update_doc)

        # print(query, update_doc)
        result = self.dbms_collection.update_one(query, update_doc, upsert)
        return result.n

    def update_many(self, query, update_doc, upsert=False):
        self.marshal_with_object_id_fix(query)
        self.marshal_with_object_id_fix(update_doc)

        result = self.dbms_collection.update_many(query, update_doc, upsert)
        return result.n

    def delete_item(self, item_id):
        query = self.oid(item_id)
        return self.delete_one(query) > 0

    def delete_one(self, query):
        if query is None:
            return 0
        result = self.dbms_collection.delete_one(query)
        return result.n

    def delete_many(self, query):
        if query is None:
            return 0
        self.marshal_with_object_id_fix(query)
        result = self.dbms_collection.delete_many(query)
        return result.n

    def find_one_and_update(self, query, update_doc, upsert=False, return_doc='after'):
        if not query:
            return None
        self.marshal_with_object_id_fix(query)
        self.marshal_with_object_id_fix(update_doc)
        return self.dbms_collection.find_one_and_update(query, update_doc, upsert=upsert, return_doc=return_doc)

    def drop(self):
        return self.dbms_collection.drop()

    def create_index(self, keys_dict, index_name):
        return self.dbms_collection.create_index(keys_dict, index_name)

    #  TODO add also other basic methods related to indexing

    def __exit__(self, type, value, traceback):
        return True


class MyDb:
    def __init__(self, dbms_db):
        self.dbms_db = dbms_db
        self.name = self.dbms_db.name
        self.c = {}

    def __getattr__(self, name):
        if name not in self.c:
            self._add_collection(name)
        return self.c[name]

    def __getitem__(self, name):
        return self.__getattr__(name)

    def __contains__(self, name):
        return name in self.c

    def _add_collection(self, cname, cache=False):
        dbms_collection = self.dbms_db.get_collection(cname)
        my_collection = MyDbCollection(dbms_collection, cache=cache)
        self.c[my_collection.name] = my_collection

    def get_collection(self, collection_name):
        return self.__getitem__(collection_name)

    # List all the collections / tables in the database
    def list(self):
        return self.c.keys()

    def list_collections(self):
        return self.dbms_db.list_collection_names()

    def drop(self):
        self.dbms_db.dbms_client.drop_database()


class MyDbClient(object):

    def __init__(self, dbms_client):
        self.dbms_client = dbms_client

    # should return MYDB/subclass instance for this client.
    def get_database(self, db_name):
        dbms_db = self.dbms_client.get_database(db_name)
        return MyDb(dbms_db)

    def list_database_names(self):
        return self.dbms_client.list_database_names()

    def drop_database(self, db_name):
        return self.dbms_client.drop_database(db_name)

    def close_connection(self):
        self.dbms_client.close()
