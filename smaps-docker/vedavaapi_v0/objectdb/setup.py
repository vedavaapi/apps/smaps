from setuptools import setup

try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except:
    long_description = ''

setup(
    name='objectdb',
    version='1.0.0',
    packages=['vedavaapi', 'vedavaapi.objectdb'],
    url='https://github.com/vedavaapi',
    author='vedavaapi',
    author_email='vvsvmb151@gmail.com',
    description='abstraction over document based dbs',
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=['pymongo', 'six'],
    classifiers=(
            "Programming Language :: Python :: 2.7",
            "Operating System :: OS Independent",
    )
)